from django.db import models
from customers.models import Customer
from django.core.urlresolvers import reverse
# Create your models here.


class Car(models.Model):

    CAR_CHOICES = (
        ('S', 'Small'),
        ('M', 'Medium'),
        ('L', 'Large'),
        ('B', 'Bike')

    )

    category = models.CharField(max_length=1, choices=CAR_CHOICES)
    model_code = models.CharField(max_length=120, unique=True)
    car_owner = models.ForeignKey(Customer)
    others_detail = models.CharField(max_length=120)
    visit_count = models.IntegerField(default=0)

    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.model_code

    def get_absolute_url(self):
        return reverse("car:detail", kwargs={"id": self.id})

    class Meta:
        ordering = ["-timestamp"]


