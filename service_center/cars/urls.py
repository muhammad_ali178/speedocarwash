from django.conf.urls import url

from .views import (
    car_create,
    car_delete,
    car_detail,
    car_list,
    car_update
)


urlpatterns = [
    url(r'^create/$', car_create, name="create"),
    url(r'^(?P<id>\d+)/$', car_detail, name="detail"),
    url(r'^$', car_list, name="list"),
    url(r'^(?P<id>\d+)/edit/$', car_update, name="update"),
    url(r'^(?P<id>\d+)/delete/$', car_delete, name="delete"),
]