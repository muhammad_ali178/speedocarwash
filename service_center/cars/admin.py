from django.contrib import admin
from .models import Car

# Register your models here.


class CarModelAdmin(admin.ModelAdmin):
    list_display = ["model_code", "updated", "timestamp"]
    search_fields = ["category", "model_code", "license_num", "color", "others_detail"]

    class Meta:
        model = Car

    # def get_model_perms(self, request):
    #     """
    #     Return empty perms dict thus hiding the model from admin index.
    #     """
    #     return {}


admin.site.register(Car, CarModelAdmin)
