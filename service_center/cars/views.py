from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.exceptions import ObjectDoesNotExist

from .models import Car

from django.contrib import messages

from .forms import CarForm

from django.core import serializers


@login_required()
def car_create(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('cars.add_car'):
        raise Http404('User has no permission to do the following task !!!')
    form = CarForm(request.POST or None)
    print(form)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully created")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "form": form,
        "title": "Create Vehicle"

    }
    return render(request, "car_form.html", context)


@login_required()
def car_detail(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    instance = get_object_or_404(Car, id=id)
    if request.is_ajax() and request.method == "GET" and request.GET['action'] == 'get_car_by_id':
        return HttpResponse(serializers.serialize('json', [instance]), content_type="application/json")
    context = {
        "car": instance,
        "title": "title list"
    }
    return render(request, "car_detail.html", context)


@login_required()
def car_list(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')

    queryset_list = Car.objects.all()

    if request.is_ajax() and request.method == "GET" and request.GET['action'] == 'get_car_for_customer':
        print("\n\n\n\n si hai")
        customer_id = request.GET['customer_id']
        try:
            queryset_list = Car.objects.filter(car_owner_id=customer_id)
            return HttpResponse(serializers.serialize('json', queryset_list), content_type="application/json")
        except ObjectDoesNotExist:
            print("Either the entry or blog doesn't exist.")

    if request.is_ajax() and request.method == "GET" and request.GET['action'] == 'get_cars':
        return HttpResponse(serializers.serialize('json', queryset_list), content_type="application/json")

    query = request.GET.get("query")
    if query:
        queryset_list = queryset_list.filter(
            Q(model_code__icontains=query) |
            Q(car_owner__full_name__contains=query) |
            Q(timestamp__icontains=query)
        ).distinct()
    paginator = Paginator(queryset_list, 10)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        query_set = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        query_set = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        query_set = paginator.page(paginator.num_pages)
    context = {
        "car_list": query_set,
        "title": "title list",
        "car_count": paginator.count
    }
    return render(request, "car_list.html", context)


@login_required()
def car_update(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('cars.change_car'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(Car, id=id)
    form = CarForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully updated")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "car": instance,
        "title": "Edit Vehicle",
        "form": form,
    }
    return render(request, "car_form.html", context)


@login_required()
def car_delete(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('cars.delete_car'):
        raise Http404('User has no permission to do the following task !!!')
    if not request.user.is_staff and not request.user.is_superuser:
        raise Http404
    instance = get_object_or_404(Car, id=id)
    instance.delete()
    messages.success(request, "successfully deleted")
    return redirect("car:list")