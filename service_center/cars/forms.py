from django import forms

from .models import Car


class CarForm(forms.ModelForm):
    class Meta:
        model = Car

        widgets = {
            'model_code': forms.TextInput(attrs={'placeholder': 'LEB - 5700 - 06'})
        }

        fields = [
            "category",
            "model_code",
            "car_owner",
            "others_detail"
        ]
