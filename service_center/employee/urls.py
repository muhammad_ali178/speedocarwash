from django.conf.urls import url

from .views import (
    employee_create,
    employee_delete,
    employee_detail,
    employee_list,
    employee_update
)


urlpatterns = [
    url(r'^create/$', employee_create, name="create"),
    url(r'^(?P<id>\d+)/$', employee_detail, name="detail"),
    url(r'^$', employee_list, name="list"),
    url(r'^(?P<id>\d+)/edit/$', employee_update, name="update"),
    url(r'^(?P<id>\d+)/delete/$', employee_delete, name="delete"),
]