from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Employee

from django.contrib import messages

from .forms import EmployeeForm


@login_required()
def employee_create(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('employee.add_employee'):
        raise Http404('User has no permission to do the following task !!!')
    form = EmployeeForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully created")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "form": form,
        "title": "Create Employee"

    }
    return render(request, "employee_form.html", context)


@login_required()
def employee_detail(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    instance = get_object_or_404(Employee, id=id)
    context = {
        "employee": instance,
        "title": "title list"
    }
    return render(request, "employee_detail.html", context)


@login_required()
def employee_list(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    queryset_list = Employee.objects.all()

    query = request.GET.get("query")
    if query:
        queryset_list = queryset_list.filter(
            Q(full_name__icontains=query) |
            Q(phone_number__icontains=query) |
            Q(timestamp__icontains=query)
        ).distinct()
    paginator = Paginator(queryset_list, 10)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        query_set = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        query_set = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        query_set = paginator.page(paginator.num_pages)
    context = {
        "employee_list": query_set,
        "title": "title list",
        "employee_count": paginator.count
    }
    return render(request, "employee_list.html", context)


@login_required()
def employee_update(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('employee.change_employee'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(Employee, id=id)
    form = EmployeeForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully updated")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "employee": instance,
        "title": "Edit Employee",
        "form": form,
    }
    return render(request, "employee_form.html", context)


@login_required()
def employee_delete(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('employee.delete_employee'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(Employee, id=id)
    instance.delete()
    messages.success(request, "successfully deleted")
    return redirect("employee:list")
