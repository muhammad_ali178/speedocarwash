from django.db import models

from phonenumber_field.modelfields import PhoneNumberField

from django.core.urlresolvers import reverse
# Create your models here.


class Employee(models.Model):
    full_name = models.CharField(max_length=120)
    phone_number = PhoneNumberField()
    address = models.CharField(max_length=50)
    city = models.CharField(max_length=60)
    salary = models.PositiveIntegerField()
    cnic_no = models.PositiveIntegerField()

    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.full_name

    def get_absolute_url(self):
        return reverse("employee:detail", kwargs={"id": self.id})

    class Meta:
        ordering = ["-timestamp"]