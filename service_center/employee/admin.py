from django.contrib import admin

from .models import Employee
# Register your models here.


class EmployeeModelAdmin(admin.ModelAdmin):
    list_display = ["full_name", "updated", "timestamp"]
    search_fields = ["full_name", "phone_number", "address", "city"]

    class Meta:
        model = Employee

admin.site.register(Employee, EmployeeModelAdmin)
