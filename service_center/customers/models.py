from django.db import models
from django.core.urlresolvers import reverse

from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.


class Customer(models.Model):

    full_name = models.CharField(max_length=120)
    phone_number = PhoneNumberField(unique=True)
    city = models.CharField(max_length=60)

    others_detail = models.CharField(max_length=100)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.full_name + "   (" + str(self.phone_number) + ")"

    def get_absolute_url(self):
        return reverse("customer:detail", kwargs={"id": self.id})

    class Meta:
        ordering = ["-timestamp"]

