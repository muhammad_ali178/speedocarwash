from django.conf.urls import url

from .views import (
    customer_create,
    customer_delete,
    customer_detail,
    customer_list,
    customer_update
)


urlpatterns = [
    url(r'^create/$', customer_create, name="create"),
    url(r'^(?P<id>\d+)/$', customer_detail, name="detail"),
    url(r'^$', customer_list, name="list"),
    url(r'^(?P<id>\d+)/edit/$', customer_update, name="update"),
    url(r'^(?P<id>\d+)/delete/$', customer_delete, name="delete"),
]