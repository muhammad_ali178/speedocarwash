from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Customer

from django.contrib import messages

from .forms import CustomerForm
from django.core import serializers
# Create your views here.


@login_required()
def customer_create(request):
    # if not request.user.is_staff:
    #     raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('customers.add_customer'):
        raise Http404('User has no permission to do the following task !!!')
    form = CustomerForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully created")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "form": form,
        "title": "Create Customer"
    }
    return render(request, "customer_form.html", context)


@login_required()
def customer_detail(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    instance = get_object_or_404(Customer, id=id)
    context = {
        "customer": instance,
        "title": "title list"
    }
    return render(request, "customer_detail.html", context)


@login_required()
def customer_list(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    queryset_list = Customer.objects.all()
    if request.method == "GET" and request.is_ajax():
        return HttpResponse(serializers.serialize('json', queryset_list), content_type="application/json")

    query = request.GET.get("query")
    if query:
        queryset_list = queryset_list.filter(
            Q(full_name__icontains=query) |
            Q(phone_number__icontains=query) |
            Q(city__icontains=query) |
            Q(others_detail__icontains=query) |
            Q(timestamp__icontains=query)

        ).distinct()
    paginator = Paginator(queryset_list, 10)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        query_set = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        query_set = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        query_set = paginator.page(paginator.num_pages)
    context = {
        "customer_list": query_set,
        "title": "title list",
        "customer_count": paginator.count
    }
    return render(request, "customer_list.html", context)


@login_required()
def customer_update(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('customers.change_customer'):
        raise Http404('User has no permission to do the following task !!!')

    instance = get_object_or_404(Customer, id=id)
    form = CustomerForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully updated")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "order": instance,
        "title": "Edit Customer",
        "form": form,
    }
    return render(request, "customer_form.html", context)


@login_required()
def customer_delete(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('customers.delete_customer'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(Customer, id=id)
    instance.delete()
    messages.success(request, "successfully deleted")
    return redirect("customer:list")