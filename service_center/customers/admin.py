from django.contrib import admin
from .models import Customer
# Register your models here.


class CustomerModelAdmin(admin.ModelAdmin):
    list_display = ["full_name", "updated", "timestamp"]
    search_fields = ["full_name", "email", "phone_number", "address", "city"]

    class Meta:
        model = Customer

admin.site.register(Customer, CustomerModelAdmin)