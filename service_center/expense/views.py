from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import ExpenseCategory,Expense

from django.contrib import messages

from .forms import ExpenseCategoryForm,ExpenseForm
from django.core import serializers
from django.http import JsonResponse
import json

@login_required()
def expense_category_create(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('expense.add_expensecategory'):
        raise Http404('User has no permission to do the following task !!!')
    form = ExpenseCategoryForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully created")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "form": form,
        "title": "Create Expense Category"
    }
    return render(request, "expense_category_form.html", context)

# @login_required()
# def expense_category_detail(request, id=None):
#     if not request.user.is_staff:
#         raise Http404('User is not authorized by admin !!!')
#     instance = get_object_or_404(ExpenseCategory, id=id)
#     context = {
#         "expense_category": instance,
#         "title": "Expense Category Detail"
#     }
#     return render(request, "expense_category_detail.html", context)

@login_required()
def expense_category_list(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    queryset_list = ExpenseCategory.objects.all()

    query = request.GET.get("query")
    if query:
        queryset_list = queryset_list.filter(
            Q(name__icontains=query) |
            Q(description__icontains=query) |
            Q(timestamp__icontains=query)
        ).distinct()
    paginator = Paginator(queryset_list, 10)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        query_set = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        query_set = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        query_set = paginator.page(paginator.num_pages)
    context = {
        "expense_category_list": query_set,
        "title": "Expense Category List",
        "expense_category_count": paginator.count
    }
    return render(request, "expense_category_list.html", context)

@login_required()
def expense_category_update(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('expense.change_expensecategory'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(ExpenseCategory, id=id)
    form = ExpenseCategoryForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully updated")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "expense_category": instance,
        "title": "Edit Expense Category",
        "form": form,
    }
    return render(request, "expense_category_form.html", context)

@login_required()
def expense_category_delete(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('expense.delete_expensecategory'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(ExpenseCategory, id=id)
    instance.delete()
    messages.success(request, "successfully deleted")
    return redirect("expense:list")

# -----------------------------------------------------------------

@login_required()
def expense_create(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('expense.add_expense'):
        raise Http404('User has no permission to do the following task !!!')
    form = ExpenseForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully created")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "form": form,
        "title": "Create Expense"
    }
    return render(request, "expense_form.html", context)

# @login_required()
# def expense_detail(request, id=None):
#     if not request.user.is_staff:
#         raise Http404('User is not authorized by admin !!!')
#     instance = get_object_or_404(Expense, id=id)
#     context = {
#         "expense": instance,
#         "title": "Expense Category Detail"
#     }
#     return render(request, "expense_detail.html", context)

@login_required()
def expense_list(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    queryset_list = Expense.objects.all()

    query = request.GET.get("query")
    if query:
        queryset_list = queryset_list.filter(
            Q(expense_category__name__contains=query) |
            Q(price__icontains=query) |
            Q(timestamp__icontains=query)
        ).distinct()
    paginator = Paginator(queryset_list, 10)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        query_set = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        query_set = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        query_set = paginator.page(paginator.num_pages)
    context = {
        "expense_list": query_set,
        "title": "Expense List",
        "expense_count": paginator.count
    }
    return render(request, "expense_list.html", context)

@login_required()
def expense_update(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('expense.change_expense'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(Expense, id=id)
    form = ExpenseForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully updated")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "expense": instance,
        "title": "Edit Expense Category",
        "form": form,
    }
    return render(request, "expense_form.html", context)

@login_required()
def expense_delete(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('expense.delete_expense'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(Expense, id=id)
    instance.delete()
    messages.success(request, "successfully deleted")
    return redirect("expense:expense_list")


