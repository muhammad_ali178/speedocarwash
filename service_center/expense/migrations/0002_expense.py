# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-10-28 15:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('expense', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Expense',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price', models.IntegerField(blank=True, default=0, null=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('expense_category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='expense.ExpenseCategory')),
            ],
            options={
                'ordering': ['-timestamp'],
            },
        ),
    ]
