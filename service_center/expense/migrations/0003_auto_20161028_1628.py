# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-10-28 16:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('expense', '0002_expense'),
    ]

    operations = [
        migrations.AlterField(
            model_name='expensecategory',
            name='name',
            field=models.CharField(max_length=120, unique=True),
        ),
    ]
