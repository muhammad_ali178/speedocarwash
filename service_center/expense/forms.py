from django import forms

from .models import ExpenseCategory,Expense


class ExpenseCategoryForm(forms.ModelForm):
    class Meta:
        model = ExpenseCategory

        fields = [
            "name",
            "description"
        ]

class ExpenseForm(forms.ModelForm):
    class Meta:
        model = Expense

        fields = [
            "expense_category",
            "price"
        ]