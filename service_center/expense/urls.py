from django.conf.urls import url

from .views import (
    expense_category_create,
    # expense_category_detail,
    expense_category_list,
    expense_category_update,
    expense_category_delete,

    expense_create,
    # expense_detail,
    expense_list,
    expense_update,
    expense_delete

)


urlpatterns = [
    url(r'^category/create/$', expense_category_create, name="create"),
    # url(r'^category/(?P<id>\d+)/$', expense_category_detail, name="detail"),
    url(r'^category/$', expense_category_list, name="list"),
    url(r'^category/(?P<id>\d+)/edit/$', expense_category_update, name="update"),
    url(r'^category/(?P<id>\d+)/delete/$', expense_category_delete, name="delete"),

    url(r'^create/$', expense_create, name="expense_create"),
    # url(r'^(?P<id>\d+)/$', expense_detail, name="expense_detail"),
    url(r'^$', expense_list, name="expense_list"),
    url(r'^(?P<id>\d+)/edit/$', expense_update, name="expense_update"),
    url(r'^(?P<id>\d+)/delete/$', expense_delete, name="expense_delete"),

]