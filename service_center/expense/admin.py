from django.contrib import admin
from .models import ExpenseCategory, Expense

# Register your models here.


class ExpenseCategoryModelAdmin(admin.ModelAdmin):
    list_display = ["name", "updated", "timestamp"]
    search_fields = ["name", "description"]

    class Meta:
        model = ExpenseCategory

admin.site.register(ExpenseCategory, ExpenseCategoryModelAdmin)

class ExpenseModelAdmin(admin.ModelAdmin):
    list_display = ["expense_category", "updated", "timestamp"]
    search_fields = ["expense_category", "price"]

    class Meta:
        model = Expense

admin.site.register(Expense, ExpenseModelAdmin)