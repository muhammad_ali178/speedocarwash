from django.db import models

from django.core.urlresolvers import reverse

# Create your models here.

class ExpenseCategory(models.Model):

    name = models.CharField(max_length=120, unique=True)
    description = models.TextField()

    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("expense:list")

    class Meta:
        ordering = ["-timestamp"]


class Expense(models.Model):
    expense_category = models.ForeignKey(ExpenseCategory)
    price = models.IntegerField(default=0, null=True, blank=True)

    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.expense_category.name

    def get_absolute_url(self):
        return reverse("expense:expense_list")

    class Meta:
        ordering = ["-timestamp"]