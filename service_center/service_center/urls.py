"""service_center URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static

from order.views import order_create

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^$', order_create, name="create"),
    url(r'^order/', include("order.urls", namespace="order")),
    url(r'^customer/', include("customers.urls", namespace="customer")),
    url(r'^employee/', include("employee.urls", namespace="employee")),
    url(r'^facility/', include("facilities.urls", namespace="facility")),
    url(r'^car/', include("cars.urls", namespace="car")),
    url(r'^reports/', include("reports.urls", namespace="reports")),
    url(r'^messages/', include("quick_messages.urls", namespace="messages")),
    url(r'^expense/', include("expense.urls", namespace="expense")),
    url(r'^discounts/', include("discounts.urls", namespace="discounts")),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = 'Speedo Car Wash'