from django.conf.urls import url

from .views import (
    sales_breakdown,
    car_count,
    expense,
    summary,
    user_own_report
)


urlpatterns = [

    url(r'^sales_breakdown/$', sales_breakdown, name="sales_breakdown"),
    url(r'^car_count/$', car_count, name="car_count"),
    url(r'^expense/$', expense, name="expense"),
    url(r'^summary/$', summary, name="summary"),
    url(r'^my_report/$', user_own_report, name="my_report"),

]