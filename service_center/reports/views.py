from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from itertools import groupby

from order.models import Order
from cars.models import Car
from facilities.models import Facilitie, Subfacilities
from expense.models import Expense,ExpenseCategory
from django.http import HttpResponse, HttpResponseRedirect, Http404
from itertools import groupby
import copy
import datetime
from datetime import timedelta
from decimal import Decimal
import calendar


# Create your views here.

@login_required()
def sales_breakdown(request):
    if not request.user.is_superuser:
        raise Http404('User has no permission to do the following task !!!')

    today = datetime.datetime.today().date()
    # end_date = datetime.datetime.strptime(str(today.__format__("%Y,%m,%d")), "%Y,%m,%d").date()
    # start_date = end_date - timedelta(days=29)
    current_month_days = calendar.monthrange(today.year, today.month)[1]
    start_date = datetime.date(today.year, today.month, 1)
    end_date = datetime.date(today.year, today.month, current_month_days)

    query = request.GET.get("query")
    if query:
        date_range_list = [x.strip() for x in query.split('!')]
        s_date = datetime.datetime.strptime(date_range_list[0], "%Y,%m,%d").date()
        e_date = datetime.datetime.strptime(date_range_list[1], "%Y,%m,%d").date()
        start_date = s_date
        end_date = e_date

    category_array = ['S', 'M', 'L', 'B']

    data_dict_array = []

    legend_dict = {}
    all_facilities = Facilitie.objects.all()
    for fac in all_facilities:
        all_sub_fac = Subfacilities.objects.filter(facility_id=fac.id)
        if all_sub_fac:
            legend_dict[fac.name] = {'facility_name': fac.name, 'sub':[]}
            for sub_fac in all_sub_fac:
                if fac.name in legend_dict:
                    legend_dict[fac.name]['sub'].append({'sub_facility_name': sub_fac.name,'sub_facility_id': sub_fac.id,
                                                         'categories':[
                        {'name': 'S', 'total_price': 0}, {'name': 'M', 'total_price': 0},
                        {'name': 'L', 'total_price': 0}, {'name': 'B', 'total_price': 0}
                    ]});

    # print(legend_dict)

    new_dict = {}

    for single_facility in all_facilities:
        new_dict[single_facility.name] = {'facility_name': single_facility.name , 'sub':[]}
        for categoty in category_array:
            orders = Order.objects.select_related().filter(approved=True, facility__name=single_facility.name,
                                                          vehicle__category=categoty, timestamp__date__range=(start_date, end_date)).order_by('-timestamp')
            for single_order in orders:
                single_order_data_dict = eval(single_order.facility_and_prices)
                date = str(single_order.timestamp.date())
                if single_facility.id in single_order_data_dict:
                    for single_sub_facility in single_order_data_dict[single_facility.id]['sub']:

                        if single_facility.name in new_dict:
                            if new_dict[single_facility.name]['sub'] != []:

                                for i, data in enumerate(new_dict[single_facility.name]['sub']):
                                    if data['date'] == date:
                                        index = i
                                        sub_dict_data = data
                                        break

                                # for index,sub_dict_data in enumerate(new_dict[single_facility.name]['sub']):

                                # ----- fix bug local variable 'sub_dict_data' referenced before assignment ---
                                try:
                                    sub_dict_data
                                except:
                                    sub_dict_data = {'date': ''}
                                # -----------------------------------------------------------------------------

                                if date in sub_dict_data['date']:
                                    for single_sub_fac_dict in sub_dict_data['sub_fac_array']:
                                        if single_sub_facility['sub_facility_id'] == single_sub_fac_dict['sub_fac_id']:
                                            single_sub_fac_dict = single_sub_fac_dict
                                            break

                                    # if any(d['sub_fac_id'] == single_sub_facility['sub_facility_id'] for d in sub_dict_data['sub_fac_array']):
                                    if single_sub_facility['sub_facility_id'] == single_sub_fac_dict['sub_fac_id']:
                                        if single_order.vehicle.category == 'S':
                                            old_price = single_sub_fac_dict['category'][0]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][0]['total_price'] = total
                                        elif single_order.vehicle.category == 'M':
                                            old_price = single_sub_fac_dict['category'][1]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][1]['total_price'] = total
                                        elif single_order.vehicle.category == 'L':
                                            old_price = single_sub_fac_dict['category'][2]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][2]['total_price'] = total
                                        else:
                                            old_price = single_sub_fac_dict['category'][3]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][3]['total_price'] = total

                                    else:
                                        # cat = single_order.vehicle.category
                                        # new_dict[single_facility.name]['sub'][index]['sub_fac_array'].append(
                                        #     {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                        #      'sub_fac_id': single_sub_facility['sub_facility_id'],
                                        #      'category': [
                                        #          {'name': 'S',
                                        #           'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                        #          {'name': 'M',
                                        #           'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                        #          {'name': 'L',
                                        #           'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                        #          {'name': 'B',
                                        #           'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                        #      ]
                                        #      })

                                        # ------------------other bug fix list index out of rage -------------

                                        cat = single_order.vehicle.category
                                        try:
                                            new_dict[single_facility.name]['sub'][index]['sub_fac_array'].append(
                                                {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                                 'sub_fac_id': single_sub_facility['sub_facility_id'],
                                                 'category': [
                                                     {'name': 'S',
                                                      'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                                     {'name': 'M',
                                                      'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                                     {'name': 'L',
                                                      'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                                     {'name': 'B',
                                                      'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                                 ]
                                                 })
                                        except:
                                            new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                                {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                                 'sub_fac_id': single_sub_facility['sub_facility_id'],
                                                 'category': [
                                                     {'name': 'S',
                                                      'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                                     {'name': 'M',
                                                      'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                                     {'name': 'L',
                                                      'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                                     {'name': 'B',
                                                      'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                                 ]
                                                 }]})
                                            # --------------------------------------------------------------------


                                else:
                                    # print(date)
                                    # print("not exist")
                                    cat = single_order.vehicle.category

                                    new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                        {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                         'sub_fac_id': single_sub_facility['sub_facility_id'],
                                         'category': [
                                             {'name': 'S',
                                              'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                             {'name': 'M',
                                              'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                             {'name': 'L',
                                              'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                             {'name': 'B',
                                              'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                         ]
                                         }]})
                                    # break

                                # break

                            else:

                                cat = single_order.vehicle.category

                                new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                    {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                     'sub_fac_id': single_sub_facility['sub_facility_id'],
                                     'category': [
                                         {'name': 'S', 'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                         {'name': 'M', 'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                         {'name': 'L', 'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                         {'name': 'B', 'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                         ]
                                     }]})



    final_dict = {}
    for key, sub_fac_of_single_facility in new_dict.items():
        for single_sub in sub_fac_of_single_facility['sub']:

            if single_sub['date'] in final_dict:
                final_dict[single_sub['date']]['facilities'].append({'facility_name': key, 'facility_data': single_sub['sub_fac_array']})
            else:
                final_dict[single_sub['date']] = {'facilities': [{'facility_name': key, 'facility_data': single_sub['sub_fac_array']}]}

    # update final dictionary with respect to facility grand total
    for key, v in final_dict.items():
        for index1,facility in enumerate(v['facilities']):
            grand_total = 0
            for index2,facility_data in enumerate(facility['facility_data']):
                for index3, category in enumerate(facility_data['category']):
                    grand_total = grand_total + category['total_price']
            final_dict[key]['facilities'][index1]['facility_grand_total'] = grand_total

    # update final dictionary with respect to all facilities grand total per day
    for key, v in final_dict.items():
        grand_total_per_day = 0
        for index1, facility in enumerate(v['facilities']):
            grand_total_per_day = grand_total_per_day + facility['facility_grand_total']
        final_dict[key]['grand_total_per_day'] = grand_total_per_day

    final_sorted_array = []
    for k,v in sorted(final_dict.items(), reverse = True):
        temp = {k:v}
        final_sorted_array.append(temp)
# --------------------------------------
    overall_grand_total = copy.deepcopy(new_dict)
    sum_dic={}
    for f_name, f_data in overall_grand_total.items():
        sum_dic[f_name]={}
        for s_f_data_index, s_f_date_data in enumerate(f_data['sub']):
            for s_f_index, s_f_data  in enumerate(s_f_date_data['sub_fac_array']):
                if s_f_data['sub_fac_id'] in sum_dic[f_name]:
                    old_s_f_data = sum_dic[f_name][s_f_data['sub_fac_id']]
                    old_s_f_data['category'][0]['total_price']= old_s_f_data['category'][0]['total_price'] + s_f_data['category'][0]['total_price']
                    old_s_f_data['category'][1]['total_price']= old_s_f_data['category'][1]['total_price'] + s_f_data['category'][1]['total_price']
                    old_s_f_data['category'][2]['total_price']= old_s_f_data['category'][2]['total_price'] + s_f_data['category'][2]['total_price']
                    old_s_f_data['category'][3]['total_price']= old_s_f_data['category'][3]['total_price'] + s_f_data['category'][3]['total_price']
                    sum_dic[f_name][s_f_data['sub_fac_id']] = old_s_f_data
                else:
                    sum_dic[f_name][s_f_data['sub_fac_id']]=s_f_data

    for key, v in sum_dic.items():
        over_all_g_total = 0
        for sub_fac_id, sub_fac_data in v.items():
            for category_index, category_data in enumerate(sub_fac_data['category']):
                over_all_g_total = over_all_g_total + category_data['total_price']
        sum_dic[key]['overall_fac_grand_total'] = over_all_g_total

    over_all_g_total_total = 0
    for key, v in sum_dic.items():
        over_all_g_total_total = over_all_g_total_total + v['overall_fac_grand_total']

    # print(over_all_g_total_total)
    facility_pie_chart = copy.deepcopy(sum_dic)
    facility_pie_chart_data = []
    for fac_name, total_sale in facility_pie_chart.items():
        facility_pie_chart_data.append({'price': total_sale['overall_fac_grand_total'], 'facility_name': fac_name})

    sub_facility_pie_chart_data = []
    for fac_name, fac_data in facility_pie_chart.items():
        for sub_id, sub_dict_data in fac_data.items():
            if type(sub_dict_data) == dict:
                total_by_category = 0
                for category in sub_dict_data['category']:
                    total_by_category = total_by_category + category['total_price']

                sub_facility_pie_chart_data.append({'sub_facility_name': sub_dict_data['sub_fac_name'], 'total_price': total_by_category})


    dates = ['x']
    sale = ['sale']
    for data_for_spline_chart in final_sorted_array:
        for date, fac_total in data_for_spline_chart.items():
            dates.append(date)
            sale.append(fac_total['grand_total_per_day'])
    spline_chart =[dates,sale]

    context = {
        'title': 'Sales Break Down',
        'data': final_sorted_array,
        'legend': legend_dict,
        'overall_grand_total': sum_dic,
        'overall_grand_total_total': over_all_g_total_total,
        'facility_pie_chart_data': facility_pie_chart_data,
        'sub_facility_pie_chart_data': sub_facility_pie_chart_data,
        'sales_per_day': spline_chart,
        'start_date': start_date,
        'end_date': end_date

    }
    return render(request, "reports_sales_breakdown.html", context)

@login_required()
def car_count(request):
    if not request.user.is_superuser:
        raise Http404('User has no permission to do the following task !!!')

    today = datetime.datetime.today().date()
    # end_date = datetime.datetime.strptime(str(today.__format__("%Y,%m,%d")), "%Y,%m,%d").date()
    # start_date = end_date - timedelta(days=29)
    current_month_days = calendar.monthrange(today.year, today.month)[1]
    start_date = datetime.date(today.year, today.month, 1)
    end_date = datetime.date(today.year, today.month, current_month_days)

    query = request.GET.get("query")
    if query:
        date_range_list = [x.strip() for x in query.split('!')]
        s_date = datetime.datetime.strptime(date_range_list[0], "%Y,%m,%d").date()
        e_date = datetime.datetime.strptime(date_range_list[1], "%Y,%m,%d").date()
        start_date = s_date
        end_date = e_date

    category_array = ['S', 'M', 'L', 'B']
    all_facilities = Facilitie.objects.all()

    legend_dict = {}
    all_facilities = Facilitie.objects.all()
    for fac in all_facilities:
        all_sub_fac = Subfacilities.objects.filter(facility_id=fac.id)
        if all_sub_fac:
            legend_dict[fac.name] = {'facility_name': fac.name, 'sub': []}
            for sub_fac in all_sub_fac:
                if fac.name in legend_dict:
                    legend_dict[fac.name]['sub'].append({'sub_facility_name': sub_fac.name, 'sub_facility_id': sub_fac.id,
                                                         'categories': [
                                                             {'name': 'S', 'total_price': 0},
                                                             {'name': 'M', 'total_price': 0},
                                                             {'name': 'L', 'total_price': 0},
                                                             {'name': 'B', 'total_price': 0}
                                                         ]});

                # print(legend_dict)

    new_dict = {}

    for single_facility in all_facilities:
        new_dict[single_facility.name] = {'facility_name': single_facility.name, 'sub': []}
        for categoty in category_array:
            orders = Order.objects.select_related().filter(approved=True, facility__name=single_facility.name,
                                                           vehicle__category=categoty, timestamp__date__range=(start_date, end_date)).order_by('-timestamp')
            for single_order in orders:
                single_order_data_dict = eval(single_order.facility_and_prices)
                date = str(single_order.timestamp.date())

                if single_facility.id in single_order_data_dict:

                    for single_sub_facility in single_order_data_dict[single_facility.id]['sub']:

                        if single_facility.name in new_dict:
                            if new_dict[single_facility.name]['sub'] != []:

                                for i, data in enumerate(new_dict[single_facility.name]['sub']):
                                    if data['date'] == date:
                                        index = i
                                        sub_dict_data = data
                                        break

                                # for index,sub_dict_data in enumerate(new_dict[single_facility.name]['sub']):

                                # ----- fix bug local variable 'sub_dict_data' referenced before assignment ---
                                try:
                                    sub_dict_data
                                except:
                                    sub_dict_data = {'date': ''}
                                # -----------------------------------------------------------------------------

                                if date in sub_dict_data['date']:
                                    for single_sub_fac_dict in sub_dict_data['sub_fac_array']:
                                        if single_sub_facility['sub_facility_id'] == single_sub_fac_dict['sub_fac_id']:
                                            single_sub_fac_dict = single_sub_fac_dict
                                            break

                                    # if any(d['sub_fac_id'] == single_sub_facility['sub_facility_id'] for d in sub_dict_data['sub_fac_array']):
                                    if single_sub_facility['sub_facility_id'] == single_sub_fac_dict['sub_fac_id']:
                                        if single_order.vehicle.category == 'S':
                                            old_price = single_sub_fac_dict['category'][0]['total_count']
                                            # new_price = single_sub_facility['price']
                                            total = old_price + 1
                                            single_sub_fac_dict['category'][0]['total_count'] = total
                                        elif single_order.vehicle.category == 'M':
                                            old_price = single_sub_fac_dict['category'][1]['total_count']
                                            # new_price = single_sub_facility['price']
                                            total = old_price + 1
                                            single_sub_fac_dict['category'][1]['total_count'] = total
                                        elif single_order.vehicle.category == 'L':
                                            old_price = single_sub_fac_dict['category'][2]['total_count']
                                            # new_price = single_sub_facility['price']
                                            total = old_price + 1
                                            single_sub_fac_dict['category'][2]['total_count'] = total
                                        else:
                                            old_price = single_sub_fac_dict['category'][3]['total_count']
                                            # new_price = single_sub_facility['price']
                                            total = old_price + 1
                                            single_sub_fac_dict['category'][3]['total_count'] = total

                                    else:
                                        # print(index)
                                        # cat = single_order.vehicle.category
                                        # new_dict[single_facility.name]['sub'][index]['sub_fac_array'].append(
                                        #     {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                        #      'sub_fac_id': single_sub_facility['sub_facility_id'],
                                        #      'category': [
                                        #          {'name': 'S',
                                        #           'total_count': 1 if cat == 'S' else 0},
                                        #          {'name': 'M',
                                        #           'total_count': 1 if cat == 'M' else 0},
                                        #          {'name': 'L',
                                        #           'total_count': 1 if cat == 'L' else 0},
                                        #          {'name': 'B',
                                        #           'total_count': 1 if cat == 'B' else 0}
                                        #      ]
                                        #      })

                                        cat = single_order.vehicle.category
                                        try:
                                            new_dict[single_facility.name]['sub'][index]['sub_fac_array'].append(
                                                {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                                 'sub_fac_id': single_sub_facility['sub_facility_id'],
                                                 'category': [
                                                     {'name': 'S',
                                                      'total_count': 1 if cat == 'S' else 0},
                                                     {'name': 'M',
                                                      'total_count': 1 if cat == 'M' else 0},
                                                     {'name': 'L',
                                                      'total_count': 1 if cat == 'L' else 0},
                                                     {'name': 'B',
                                                      'total_count': 1 if cat == 'B' else 0}
                                                 ]
                                                 })
                                        except:
                                            new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                                {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                                 'sub_fac_id': single_sub_facility['sub_facility_id'],
                                                 'category': [
                                                     {'name': 'S',
                                                      'total_count': 1 if cat == 'S' else 0},
                                                     {'name': 'M',
                                                      'total_count': 1 if cat == 'M' else 0},
                                                     {'name': 'L',
                                                      'total_count': 1 if cat == 'L' else 0},
                                                     {'name': 'B',
                                                      'total_count': 1 if cat == 'B' else 0}
                                                 ]
                                                 }]})



                                else:
                                    # print(date)
                                    # print("not exist")
                                    cat = single_order.vehicle.category

                                    new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                        {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                         'sub_fac_id': single_sub_facility['sub_facility_id'],
                                         'category': [
                                             {'name': 'S',
                                              'total_count': 1 if cat == 'S' else 0},
                                             {'name': 'M',
                                              'total_count': 1 if cat == 'M' else 0},
                                             {'name': 'L',
                                              'total_count': 1 if cat == 'L' else 0},
                                             {'name': 'B',
                                              'total_count': 1 if cat == 'B' else 0}
                                         ]
                                         }]})

                            else:

                                cat = single_order.vehicle.category

                                new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                    {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                     'sub_fac_id': single_sub_facility['sub_facility_id'],
                                     'category': [
                                         {'name': 'S', 'total_count': 1 if cat == 'S' else 0},
                                         {'name': 'M', 'total_count': 1 if cat == 'M' else 0},
                                         {'name': 'L', 'total_count': 1 if cat == 'L' else 0},
                                         {'name': 'B', 'total_count': 1 if cat == 'B' else 0}
                                     ]
                                     }]})
    # print(new_dict)

    final_dict = {}
    for key, sub_fac_of_single_facility in new_dict.items():
        for single_sub in sub_fac_of_single_facility['sub']:

            if single_sub['date'] in final_dict:
                final_dict[single_sub['date']]['facilities'].append(
                    {'facility_name': key, 'facility_data': single_sub['sub_fac_array']})
            else:
                final_dict[single_sub['date']] = {
                    'facilities': [{'facility_name': key, 'facility_data': single_sub['sub_fac_array']}]}

                # update final dictionary with respect to facility grand total
    for key, v in final_dict.items():
        for index1, facility in enumerate(v['facilities']):
            grand_total = 0
            for index2, facility_data in enumerate(facility['facility_data']):
                for index3, category in enumerate(facility_data['category']):
                    grand_total = grand_total + category['total_count']
            final_dict[key]['facilities'][index1]['facility_grand_total'] = grand_total

            # update final dictionary with respect to all facilities grand total per day
    for key, v in final_dict.items():
        grand_total_per_day = 0
        for index1, facility in enumerate(v['facilities']):
            grand_total_per_day = grand_total_per_day + facility['facility_grand_total']
        final_dict[key]['grand_total_per_day'] = grand_total_per_day

    final_sorted_array = []
    for k, v in sorted(final_dict.items(), reverse=True):
        temp = {k: v}
        final_sorted_array.append(temp)

    # print(final_sorted_array)

    # --------------------------------------
    overall_grand_total = copy.deepcopy(new_dict)
    sum_dic = {}
    for f_name, f_data in overall_grand_total.items():
        sum_dic[f_name] = {}
        for s_f_data_index, s_f_date_data in enumerate(f_data['sub']):
            for s_f_index, s_f_data in enumerate(s_f_date_data['sub_fac_array']):
                if s_f_data['sub_fac_id'] in sum_dic[f_name]:
                    old_s_f_data = sum_dic[f_name][s_f_data['sub_fac_id']]
                    old_s_f_data['category'][0]['total_count'] = old_s_f_data['category'][0]['total_count'] + \
                                                                 s_f_data['category'][0]['total_count']
                    old_s_f_data['category'][1]['total_count'] = old_s_f_data['category'][1]['total_count'] + \
                                                                 s_f_data['category'][1]['total_count']
                    old_s_f_data['category'][2]['total_count'] = old_s_f_data['category'][2]['total_count'] + \
                                                                 s_f_data['category'][2]['total_count']
                    old_s_f_data['category'][3]['total_count'] = old_s_f_data['category'][3]['total_count'] + \
                                                                 s_f_data['category'][3]['total_count']
                    sum_dic[f_name][s_f_data['sub_fac_id']] = old_s_f_data
                else:
                    sum_dic[f_name][s_f_data['sub_fac_id']] = s_f_data

    for key, v in sum_dic.items():
        over_all_g_total = 0
        for sub_fac_id, sub_fac_data in v.items():
            for category_index, category_data in enumerate(sub_fac_data['category']):
                over_all_g_total = over_all_g_total + category_data['total_count']
        sum_dic[key]['overall_fac_grand_total'] = over_all_g_total

    over_all_g_total_total = 0
    for key, v in sum_dic.items():
        over_all_g_total_total = over_all_g_total_total + v['overall_fac_grand_total']

    context = {
        'title': 'Car Count',
        'data': final_sorted_array,
        'legend': legend_dict,
        'overall_grand_total': sum_dic,
        'overall_grand_total_total': over_all_g_total_total,
        'start_date': start_date,
        'end_date': end_date

    }
    return render(request, "reports_car_count.html", context)

@login_required()
def expense(request):
    if not request.user.is_superuser:
        raise Http404('User has no permission to do the following task !!!')

    today = datetime.datetime.today().date()
    # end_date = datetime.datetime.strptime(str(today.__format__("%Y,%m,%d")), "%Y,%m,%d").date()
    # start_date = end_date - timedelta(days=29)
    current_month_days = calendar.monthrange(today.year, today.month)[1]
    start_date = datetime.date(today.year, today.month, 1)
    end_date = datetime.date(today.year, today.month, current_month_days)

    query = request.GET.get("query")
    if query:
        date_range_list = [x.strip() for x in query.split('!')]
        s_date = datetime.datetime.strptime(date_range_list[0], "%Y,%m,%d").date()
        e_date = datetime.datetime.strptime(date_range_list[1], "%Y,%m,%d").date()
        start_date = s_date
        end_date = e_date

    all_expense_category_data = ExpenseCategory.objects.all()
    lagend_expense_list = []
    for legend_expense in all_expense_category_data:
        lagend_expense_list.append(legend_expense.name)

    all_expenses = Expense.objects.select_related().filter(timestamp__date__range=(start_date, end_date)).order_by('-timestamp')

    expense_dict = {}
    for single_expense in all_expenses:
        date = str(single_expense.timestamp.date())
        if date in expense_dict:
            category_index = data = None
            for index,category_data in enumerate(expense_dict[date]['categories']):

                if single_expense.expense_category.name == category_data['name']:
                    category_index = index
                    data = category_data
            if not category_index == None:
                old_price = data['price']
                new_price = single_expense.price
                updated_price = old_price+new_price
                expense_dict[date]['categories'][category_index]['price'] = updated_price

            else:
                expense_dict[date]['categories'].append({'name': single_expense.expense_category.name, 'price': single_expense.price})
        else:
            expense_dict[date] = {'categories':[{'name': single_expense.expense_category.name, 'price': single_expense.price}]}

    expense_sorted_array = []
    for k, v in sorted(expense_dict.items(), reverse=True):
        temp = {k: v}
        expense_sorted_array.append(temp)

    overall_total_per_day = copy.deepcopy(expense_dict)
    for k, v in overall_total_per_day.items():
        total_per_day = 0
        for single_cat in v['categories']:
            total_per_day = total_per_day + single_cat['price']
        expense_dict[k]['total_per_day'] = total_per_day

    overall_total_per_category = copy.deepcopy(expense_dict)
    overall_total_per_cat_dict = {}
    for k, v in overall_total_per_category.items():
        for s_cat_data in v['categories']:
            if s_cat_data['name'] in overall_total_per_cat_dict:
                old_cat_total = overall_total_per_cat_dict[s_cat_data['name']]
                overall_total_per_cat_dict[s_cat_data['name']] = old_cat_total + s_cat_data['price']
            else:
                overall_total_per_cat_dict[s_cat_data['name']] = s_cat_data['price']

    overall_expense_grand_total = copy.deepcopy(overall_total_per_cat_dict)
    expense_grand_total = 0
    for k,v in overall_expense_grand_total.items():
        expense_grand_total = expense_grand_total + v

    context = {
        'title': 'daily expense',
        'lagend_expense_categories': lagend_expense_list,
        'expense_data': expense_sorted_array,
        'overall_total_per_cat_dict': overall_total_per_cat_dict,
        'expense_grand_total':expense_grand_total,
        'start_date': start_date,
        'end_date': end_date

    }
    return render(request, "reports_expense.html", context)


@login_required()
def summary(request):
    if not request.user.is_superuser:
        raise Http404('User has no permission to do the following task !!!')

    today = datetime.datetime.today().date()
    # end_date = datetime.datetime.strptime(str(today.__format__("%Y,%m,%d")), "%Y,%m,%d").date()
    # start_date = end_date - timedelta(days=29)
    current_month_days = calendar.monthrange(today.year, today.month)[1]
    start_date = datetime.date(today.year, today.month, 1)
    end_date = datetime.date(today.year, today.month, current_month_days)

    query = request.GET.get("query")
    if query:
        date_range_list = [x.strip() for x in query.split('!')]
        s_date = datetime.datetime.strptime(date_range_list[0], "%Y,%m,%d").date()
        e_date = datetime.datetime.strptime(date_range_list[1], "%Y,%m,%d").date()
        start_date = s_date
        end_date = e_date

    category_array = ['S', 'M', 'L', 'B']

    data_dict_array = []

    legend_dict = {}
    all_facilities = Facilitie.objects.all()
    for fac in all_facilities:
        legend_dict[fac.name] = {'facility_name': fac.name, 'sub': []}
        all_sub_fac = Subfacilities.objects.filter(facility_id=fac.id)
        for sub_fac in all_sub_fac:
            if fac.name in legend_dict:
                legend_dict[fac.name]['sub'].append({'sub_facility_name': sub_fac.name, 'sub_facility_id': sub_fac.id,
                                                     'categories': [
                                                         {'name': 'S', 'total_price': 0},
                                                         {'name': 'M', 'total_price': 0},
                                                         {'name': 'L', 'total_price': 0},
                                                         {'name': 'B', 'total_price': 0}
                                                     ]});

    # print(legend_dict)

    new_dict = {}

    for single_facility in all_facilities:
        new_dict[single_facility.name] = {'facility_name': single_facility.name, 'sub': []}
        for categoty in category_array:
            orders = Order.objects.select_related().filter(approved=True, facility__name=single_facility.name,
                                                           vehicle__category=categoty,
                                                           timestamp__date__range=(start_date, end_date)).order_by(
                '-timestamp')
            for single_order in orders:
                single_order_data_dict = eval(single_order.facility_and_prices)
                date = str(single_order.timestamp.date())

                if single_facility.id in single_order_data_dict:

                    for single_sub_facility in single_order_data_dict[single_facility.id]['sub']:

                        if single_facility.name in new_dict:
                            if new_dict[single_facility.name]['sub'] != []:

                                for i, data in enumerate(new_dict[single_facility.name]['sub']):
                                    if data['date'] == date:
                                        index = i
                                        sub_dict_data = data
                                        break

                                # for index,sub_dict_data in enumerate(new_dict[single_facility.name]['sub']):

                                # ----- fix bug local variable 'sub_dict_data' referenced before assignment ---
                                try:
                                    sub_dict_data
                                except:
                                    sub_dict_data = {'date': ''}
                                # -----------------------------------------------------------------------------

                                if date in sub_dict_data['date']:
                                    for single_sub_fac_dict in sub_dict_data['sub_fac_array']:
                                        if single_sub_facility['sub_facility_id'] == single_sub_fac_dict['sub_fac_id']:
                                            single_sub_fac_dict = single_sub_fac_dict
                                            break

                                    # if any(d['sub_fac_id'] == single_sub_facility['sub_facility_id'] for d in sub_dict_data['sub_fac_array']):
                                    if single_sub_facility['sub_facility_id'] == single_sub_fac_dict['sub_fac_id']:
                                        if single_order.vehicle.category == 'S':
                                            old_price = single_sub_fac_dict['category'][0]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][0]['total_price'] = total
                                        elif single_order.vehicle.category == 'M':
                                            old_price = single_sub_fac_dict['category'][1]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][1]['total_price'] = total
                                        elif single_order.vehicle.category == 'L':
                                            old_price = single_sub_fac_dict['category'][2]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][2]['total_price'] = total
                                        else:
                                            old_price = single_sub_fac_dict['category'][3]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][3]['total_price'] = total

                                    else:
                                        # print(index)
                                        # cat = single_order.vehicle.category
                                        # new_dict[single_facility.name]['sub'][index]['sub_fac_array'].append(
                                        #     {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                        #      'sub_fac_id': single_sub_facility['sub_facility_id'],
                                        #      'category': [
                                        #          {'name': 'S',
                                        #           'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                        #          {'name': 'M',
                                        #           'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                        #          {'name': 'L',
                                        #           'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                        #          {'name': 'B',
                                        #           'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                        #      ]
                                        #      })
                                        cat = single_order.vehicle.category
                                        try:
                                            new_dict[single_facility.name]['sub'][index]['sub_fac_array'].append(
                                                {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                                 'sub_fac_id': single_sub_facility['sub_facility_id'],
                                                 'category': [
                                                     {'name': 'S',
                                                      'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                                     {'name': 'M',
                                                      'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                                     {'name': 'L',
                                                      'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                                     {'name': 'B',
                                                      'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                                 ]
                                                 })
                                        except:
                                            new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                                {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                                 'sub_fac_id': single_sub_facility['sub_facility_id'],
                                                 'category': [
                                                     {'name': 'S',
                                                      'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                                     {'name': 'M',
                                                      'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                                     {'name': 'L',
                                                      'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                                     {'name': 'B',
                                                      'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                                 ]
                                                 }]})


                                else:
                                    # print(date)
                                    # print("not exist")
                                    cat = single_order.vehicle.category

                                    new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                        {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                         'sub_fac_id': single_sub_facility['sub_facility_id'],
                                         'category': [
                                             {'name': 'S',
                                              'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                             {'name': 'M',
                                              'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                             {'name': 'L',
                                              'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                             {'name': 'B',
                                              'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                         ]
                                         }]})
                                    # break

                                    # break

                            else:

                                cat = single_order.vehicle.category

                                new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                    {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                     'sub_fac_id': single_sub_facility['sub_facility_id'],
                                     'category': [
                                         {'name': 'S', 'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                         {'name': 'M', 'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                         {'name': 'L', 'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                         {'name': 'B', 'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                     ]
                                     }]})

    final_dict = {}
    for key, sub_fac_of_single_facility in new_dict.items():
        for single_sub in sub_fac_of_single_facility['sub']:

            if single_sub['date'] in final_dict:
                final_dict[single_sub['date']]['facilities'].append(
                    {'facility_name': key, 'facility_data': single_sub['sub_fac_array']})
            else:
                final_dict[single_sub['date']] = {
                    'facilities': [{'facility_name': key, 'facility_data': single_sub['sub_fac_array']}]}

    # update final dictionary with respect to facility grand total
    for key, v in final_dict.items():
        for index1, facility in enumerate(v['facilities']):
            grand_total = 0
            for index2, facility_data in enumerate(facility['facility_data']):
                for index3, category in enumerate(facility_data['category']):
                    grand_total = grand_total + category['total_price']
            final_dict[key]['facilities'][index1]['facility_grand_total'] = grand_total

    # update final dictionary with respect to all facilities grand total per day
    for key, v in final_dict.items():
        grand_total_per_day = 0
        for index1, facility in enumerate(v['facilities']):
            grand_total_per_day = grand_total_per_day + facility['facility_grand_total']
        final_dict[key]['grand_total_per_day'] = grand_total_per_day

    final_sorted_array = []
    for k, v in sorted(final_dict.items(), reverse=True):
        temp = {k: v}
        final_sorted_array.append(temp)
        # --------------------------------------
    overall_grand_total = copy.deepcopy(new_dict)
    sum_dic = {}
    for f_name, f_data in overall_grand_total.items():
        sum_dic[f_name] = {}
        for s_f_data_index, s_f_date_data in enumerate(f_data['sub']):
            for s_f_index, s_f_data in enumerate(s_f_date_data['sub_fac_array']):
                if s_f_data['sub_fac_id'] in sum_dic[f_name]:
                    old_s_f_data = sum_dic[f_name][s_f_data['sub_fac_id']]
                    old_s_f_data['category'][0]['total_price'] = old_s_f_data['category'][0]['total_price'] + \
                                                                 s_f_data['category'][0]['total_price']
                    old_s_f_data['category'][1]['total_price'] = old_s_f_data['category'][1]['total_price'] + \
                                                                 s_f_data['category'][1]['total_price']
                    old_s_f_data['category'][2]['total_price'] = old_s_f_data['category'][2]['total_price'] + \
                                                                 s_f_data['category'][2]['total_price']
                    old_s_f_data['category'][3]['total_price'] = old_s_f_data['category'][3]['total_price'] + \
                                                                 s_f_data['category'][3]['total_price']
                    sum_dic[f_name][s_f_data['sub_fac_id']] = old_s_f_data
                else:
                    sum_dic[f_name][s_f_data['sub_fac_id']] = s_f_data

    for key, v in sum_dic.items():
        over_all_g_total = 0
        for sub_fac_id, sub_fac_data in v.items():
            for category_index, category_data in enumerate(sub_fac_data['category']):
                over_all_g_total = over_all_g_total + category_data['total_price']
        sum_dic[key]['overall_fac_grand_total'] = over_all_g_total

    over_all_g_total_total = 0
    for key, v in sum_dic.items():
        over_all_g_total_total = over_all_g_total_total + v['overall_fac_grand_total']

    summary_sales = copy.deepcopy(sum_dic)
    sales_revenue = {}
    for name, fac_data in summary_sales.items():
        sales_revenue[name] = {'total' : fac_data['overall_fac_grand_total']}

    if not over_all_g_total_total == 0:
        for name, total in sales_revenue.items():
            percentage = (total['total'] / over_all_g_total_total) * 100
            decimal_percentage = Decimal(percentage)
            single_percentage = str(round(decimal_percentage,2))
            sales_revenue[name]['over_all_percentage'] = single_percentage

    # ----------------------- calculate income with above code
    # ----------------------- calculate expense with below code

    query = request.GET.get("query")
    if query:
        date_range_list = [x.strip() for x in query.split('!')]
        s_date = datetime.datetime.strptime(date_range_list[0], "%Y,%m,%d").date()
        e_date = datetime.datetime.strptime(date_range_list[1], "%Y,%m,%d").date()
        start_date = s_date
        end_date = e_date

    all_expense_category_data = ExpenseCategory.objects.all()
    lagend_expense_list = []
    for legend_expense in all_expense_category_data:
        lagend_expense_list.append(legend_expense.name)

    all_expenses = Expense.objects.select_related().filter(timestamp__date__range=(start_date, end_date)).order_by(
        '-timestamp')

    expense_dict = {}
    for single_expense in all_expenses:
        date = str(single_expense.timestamp.date())
        if date in expense_dict:
            category_index = data = None
            for index, category_data in enumerate(expense_dict[date]['categories']):

                if single_expense.expense_category.name == category_data['name']:
                    category_index = index
                    data = category_data
            if not category_index == None:
                old_price = data['price']
                new_price = single_expense.price
                updated_price = old_price + new_price
                expense_dict[date]['categories'][category_index]['price'] = updated_price

            else:
                expense_dict[date]['categories'].append(
                    {'name': single_expense.expense_category.name, 'price': single_expense.price})
        else:
            expense_dict[date] = {
                'categories': [{'name': single_expense.expense_category.name, 'price': single_expense.price}]}

    expense_sorted_array = []
    for k, v in sorted(expense_dict.items(), reverse=True):
        temp = {k: v}
        expense_sorted_array.append(temp)

    overall_total_per_day = copy.deepcopy(expense_dict)
    for k, v in overall_total_per_day.items():
        total_per_day = 0
        for single_cat in v['categories']:
            total_per_day = total_per_day + single_cat['price']
        expense_dict[k]['total_per_day'] = total_per_day

    overall_total_per_category = copy.deepcopy(expense_dict)
    overall_total_per_cat_dict = {}
    for k, v in overall_total_per_category.items():
        for s_cat_data in v['categories']:
            if s_cat_data['name'] in overall_total_per_cat_dict:
                old_cat_total = overall_total_per_cat_dict[s_cat_data['name']]
                overall_total_per_cat_dict[s_cat_data['name']] = old_cat_total + s_cat_data['price']
            else:
                overall_total_per_cat_dict[s_cat_data['name']] = s_cat_data['price']

    overall_expense_grand_total = copy.deepcopy(overall_total_per_cat_dict)
    expense_grand_total = 0
    for k, v in overall_expense_grand_total.items():
        expense_grand_total = expense_grand_total + v

    expense_dic_per_cat = copy.deepcopy(overall_total_per_cat_dict)
    expense_data_dict = {}
    for name, fac_data in expense_dic_per_cat.items():
        expense_data_dict[name] = {'total': fac_data}

    if not expense_grand_total == 0:
        for name, total in expense_dic_per_cat.items():
            # % costs
            percentage = (total / expense_grand_total) * 100
            decimal_percentage = Decimal(percentage)
            single_percentage = str(round(decimal_percentage, 2))
            expense_data_dict[name]['over_all_expense_percentage'] = single_percentage

            # % of sales of total cost per category
            percentage_of_sales_of_cost = (total / over_all_g_total_total) * 100
            decimal_percentage_of_sales_of_cost = Decimal(percentage_of_sales_of_cost)
            single_percentage_of_sales_of_cost = str(round(decimal_percentage_of_sales_of_cost, 2))
            expense_data_dict[name]['over_all_expense_percentage_of_sales'] = single_percentage_of_sales_of_cost

    # calculate total % sale of total cost
    total_expense_percentage_sale = 0
    for ex_single_cost_name, ex_single_cost_data in expense_data_dict.items():
            total_expense_percentage_sale = total_expense_percentage_sale + float(ex_single_cost_data['over_all_expense_percentage_of_sales'])
    total_expense_percentage_sale_decimal = str(round(Decimal(total_expense_percentage_sale),2))


    total_income = over_all_g_total_total
    total_expenses = expense_grand_total

    net_calculations = {}
    if total_income > total_expenses:
        print("profit")
        profit = total_income - total_expenses
        if not total_income == 0:
            profit_percentage = str(round(Decimal((profit / total_income) * 100),2))
            net_calculations['result'] = {'data_summary': 'Net Profit', 'total_value': profit, 'value_in_percentage': profit_percentage }
        else:
            net_calculations['result'] = {'data_summary': 'Net Profit', 'total_value': profit,
                                          'value_in_percentage': 100.00}

    elif total_expenses > total_income:
        loss = total_expenses - total_income

        if not total_income == 0:
            loss_percentage = str(round(Decimal((loss / total_income) * 100), 2))
            net_calculations['result'] = {'data_summary': 'Net Loss', 'total_value': loss, 'value_in_percentage': loss_percentage}
        else:
            net_calculations['result'] = {'data_summary': 'Net Loss', 'total_value': loss, 'value_in_percentage': 100.00}

    context = {
        'title': 'Profit & Loss',
        'lagend_expense_categories': lagend_expense_list,
        'sales_revenue': sales_revenue,
        'over_all_g_total_total': over_all_g_total_total,
        'expense_data': expense_data_dict,
        'expense_grand_total': expense_grand_total,
        'net_calculations': net_calculations,
        'total_expense_percentage_sale' : total_expense_percentage_sale_decimal,
        'start_date': start_date,
        'end_date': end_date
    }
    return render(request, "summary.html", context)

@login_required()
def user_own_report(request):
    if not (request.user.is_superuser or request.user.is_staff):
        raise Http404('User has no permission to do the following task !!!')

    today = datetime.datetime.today().date()
    # end_date = datetime.datetime.strptime(str(today.__format__("%Y,%m,%d")), "%Y,%m,%d").date()
    # start_date = end_date - timedelta(days=29)
    current_month_days = calendar.monthrange(today.year, today.month)[1]
    start_date = datetime.date(today.year, today.month, 1)
    end_date = datetime.date(today.year, today.month, current_month_days)

    query = request.GET.get("query")
    if query:
        date_range_list = [x.strip() for x in query.split('!')]
        s_date = datetime.datetime.strptime(date_range_list[0], "%Y,%m,%d").date()
        e_date = datetime.datetime.strptime(date_range_list[1], "%Y,%m,%d").date()
        start_date = s_date
        end_date = e_date

    category_array = ['S', 'M', 'L', 'B']

    data_dict_array = []

    legend_dict = {}
    all_facilities = Facilitie.objects.all()
    for fac in all_facilities:
        all_sub_fac = Subfacilities.objects.filter(facility_id=fac.id)
        if all_sub_fac:
            legend_dict[fac.name] = {'facility_name': fac.name, 'sub':[]}
            for sub_fac in all_sub_fac:
                if fac.name in legend_dict:
                    legend_dict[fac.name]['sub'].append({'sub_facility_name': sub_fac.name,'sub_facility_id': sub_fac.id,
                                                         'categories':[
                        {'name': 'S', 'total_price': 0}, {'name': 'M', 'total_price': 0},
                        {'name': 'L', 'total_price': 0}, {'name': 'B', 'total_price': 0}
                    ]});

    # print(legend_dict)

    new_dict = {}

    for single_facility in all_facilities:
        new_dict[single_facility.name] = {'facility_name': single_facility.name , 'sub':[]}
        for categoty in category_array:
            orders = Order.objects.select_related().filter(approved=True, facility__name=single_facility.name,
                                                          vehicle__category=categoty, created_user_id=request.user.id, timestamp__date__range=(start_date, end_date)).order_by('-timestamp')
            for single_order in orders:
                single_order_data_dict = eval(single_order.facility_and_prices)
                date = str(single_order.timestamp.date())
                if single_facility.id in single_order_data_dict:
                    for single_sub_facility in single_order_data_dict[single_facility.id]['sub']:

                        if single_facility.name in new_dict:
                            if new_dict[single_facility.name]['sub'] != []:

                                for i, data in enumerate(new_dict[single_facility.name]['sub']):
                                    if data['date'] == date:
                                        index = i
                                        sub_dict_data = data
                                        break

                                # for index,sub_dict_data in enumerate(new_dict[single_facility.name]['sub']):

                                # ----- fix bug local variable 'sub_dict_data' referenced before assignment ---
                                try:
                                    sub_dict_data
                                except:
                                    sub_dict_data = {'date': ''}
                                # -----------------------------------------------------------------------------

                                if date in sub_dict_data['date']:
                                    for single_sub_fac_dict in sub_dict_data['sub_fac_array']:
                                        if single_sub_facility['sub_facility_id'] == single_sub_fac_dict['sub_fac_id']:
                                            single_sub_fac_dict = single_sub_fac_dict
                                            break

                                    # if any(d['sub_fac_id'] == single_sub_facility['sub_facility_id'] for d in sub_dict_data['sub_fac_array']):
                                    if single_sub_facility['sub_facility_id'] == single_sub_fac_dict['sub_fac_id']:
                                        if single_order.vehicle.category == 'S':
                                            old_price = single_sub_fac_dict['category'][0]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][0]['total_price'] = total
                                        elif single_order.vehicle.category == 'M':
                                            old_price = single_sub_fac_dict['category'][1]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][1]['total_price'] = total
                                        elif single_order.vehicle.category == 'L':
                                            old_price = single_sub_fac_dict['category'][2]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][2]['total_price'] = total
                                        else:
                                            old_price = single_sub_fac_dict['category'][3]['total_price']
                                            new_price = single_sub_facility['price']
                                            total = old_price + new_price
                                            single_sub_fac_dict['category'][3]['total_price'] = total

                                    else:
                                        # cat = single_order.vehicle.category
                                        # new_dict[single_facility.name]['sub'][index]['sub_fac_array'].append(
                                        #     {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                        #      'sub_fac_id': single_sub_facility['sub_facility_id'],
                                        #      'category': [
                                        #          {'name': 'S',
                                        #           'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                        #          {'name': 'M',
                                        #           'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                        #          {'name': 'L',
                                        #           'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                        #          {'name': 'B',
                                        #           'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                        #      ]
                                        #      })

                                        # ------------------other bug fix list index out of rage -------------

                                        cat = single_order.vehicle.category
                                        try:
                                            new_dict[single_facility.name]['sub'][index]['sub_fac_array'].append(
                                                {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                                 'sub_fac_id': single_sub_facility['sub_facility_id'],
                                                 'category': [
                                                     {'name': 'S',
                                                      'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                                     {'name': 'M',
                                                      'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                                     {'name': 'L',
                                                      'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                                     {'name': 'B',
                                                      'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                                 ]
                                                 })
                                        except:
                                            new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                                {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                                 'sub_fac_id': single_sub_facility['sub_facility_id'],
                                                 'category': [
                                                     {'name': 'S',
                                                      'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                                     {'name': 'M',
                                                      'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                                     {'name': 'L',
                                                      'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                                     {'name': 'B',
                                                      'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                                 ]
                                                 }]})
                                            # --------------------------------------------------------------------


                                else:
                                    # print(date)
                                    # print("not exist")
                                    cat = single_order.vehicle.category

                                    new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                        {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                         'sub_fac_id': single_sub_facility['sub_facility_id'],
                                         'category': [
                                             {'name': 'S',
                                              'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                             {'name': 'M',
                                              'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                             {'name': 'L',
                                              'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                             {'name': 'B',
                                              'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                         ]
                                         }]})
                                    # break

                                # break

                            else:

                                cat = single_order.vehicle.category

                                new_dict[single_facility.name]['sub'].append({'date': date, 'sub_fac_array': [
                                    {'sub_fac_name': single_sub_facility['sub_facility_name'],
                                     'sub_fac_id': single_sub_facility['sub_facility_id'],
                                     'category': [
                                         {'name': 'S', 'total_price': single_sub_facility['price'] if cat == 'S' else 0},
                                         {'name': 'M', 'total_price': single_sub_facility['price'] if cat == 'M' else 0},
                                         {'name': 'L', 'total_price': single_sub_facility['price'] if cat == 'L' else 0},
                                         {'name': 'B', 'total_price': single_sub_facility['price'] if cat == 'B' else 0}
                                         ]
                                     }]})



    final_dict = {}
    for key, sub_fac_of_single_facility in new_dict.items():
        for single_sub in sub_fac_of_single_facility['sub']:

            if single_sub['date'] in final_dict:
                final_dict[single_sub['date']]['facilities'].append({'facility_name': key, 'facility_data': single_sub['sub_fac_array']})
            else:
                final_dict[single_sub['date']] = {'facilities': [{'facility_name': key, 'facility_data': single_sub['sub_fac_array']}]}

    # update final dictionary with respect to facility grand total
    for key, v in final_dict.items():
        for index1,facility in enumerate(v['facilities']):
            grand_total = 0
            for index2,facility_data in enumerate(facility['facility_data']):
                for index3, category in enumerate(facility_data['category']):
                    grand_total = grand_total + category['total_price']
            final_dict[key]['facilities'][index1]['facility_grand_total'] = grand_total

    # update final dictionary with respect to all facilities grand total per day
    for key, v in final_dict.items():
        grand_total_per_day = 0
        for index1, facility in enumerate(v['facilities']):
            grand_total_per_day = grand_total_per_day + facility['facility_grand_total']
        final_dict[key]['grand_total_per_day'] = grand_total_per_day

    final_sorted_array = []
    for k,v in sorted(final_dict.items(), reverse = True):
        temp = {k:v}
        final_sorted_array.append(temp)
# --------------------------------------
    overall_grand_total = copy.deepcopy(new_dict)
    sum_dic={}
    for f_name, f_data in overall_grand_total.items():
        sum_dic[f_name]={}
        for s_f_data_index, s_f_date_data in enumerate(f_data['sub']):
            for s_f_index, s_f_data  in enumerate(s_f_date_data['sub_fac_array']):
                if s_f_data['sub_fac_id'] in sum_dic[f_name]:
                    old_s_f_data = sum_dic[f_name][s_f_data['sub_fac_id']]
                    old_s_f_data['category'][0]['total_price']= old_s_f_data['category'][0]['total_price'] + s_f_data['category'][0]['total_price']
                    old_s_f_data['category'][1]['total_price']= old_s_f_data['category'][1]['total_price'] + s_f_data['category'][1]['total_price']
                    old_s_f_data['category'][2]['total_price']= old_s_f_data['category'][2]['total_price'] + s_f_data['category'][2]['total_price']
                    old_s_f_data['category'][3]['total_price']= old_s_f_data['category'][3]['total_price'] + s_f_data['category'][3]['total_price']
                    sum_dic[f_name][s_f_data['sub_fac_id']] = old_s_f_data
                else:
                    sum_dic[f_name][s_f_data['sub_fac_id']]=s_f_data

    for key, v in sum_dic.items():
        over_all_g_total = 0
        for sub_fac_id, sub_fac_data in v.items():
            for category_index, category_data in enumerate(sub_fac_data['category']):
                over_all_g_total = over_all_g_total + category_data['total_price']
        sum_dic[key]['overall_fac_grand_total'] = over_all_g_total

    over_all_g_total_total = 0
    for key, v in sum_dic.items():
        over_all_g_total_total = over_all_g_total_total + v['overall_fac_grand_total']

    # print(over_all_g_total_total)
    facility_pie_chart = copy.deepcopy(sum_dic)
    facility_pie_chart_data = []
    for fac_name, total_sale in facility_pie_chart.items():
        facility_pie_chart_data.append({'price': total_sale['overall_fac_grand_total'], 'facility_name': fac_name})

    sub_facility_pie_chart_data = []
    for fac_name, fac_data in facility_pie_chart.items():
        for sub_id, sub_dict_data in fac_data.items():
            if type(sub_dict_data) == dict:
                total_by_category = 0
                for category in sub_dict_data['category']:
                    total_by_category = total_by_category + category['total_price']

                sub_facility_pie_chart_data.append({'sub_facility_name': sub_dict_data['sub_fac_name'], 'total_price': total_by_category})


    dates = ['x']
    sale = ['sale']
    for data_for_spline_chart in final_sorted_array:
        for date, fac_total in data_for_spline_chart.items():
            dates.append(date)
            sale.append(fac_total['grand_total_per_day'])
    spline_chart =[dates,sale]

    context = {
        'title': 'Sales Break Down',
        'data': final_sorted_array,
        'legend': legend_dict,
        'overall_grand_total': sum_dic,
        'overall_grand_total_total': over_all_g_total_total,
        'facility_pie_chart_data': facility_pie_chart_data,
        'sub_facility_pie_chart_data': sub_facility_pie_chart_data,
        'sales_per_day': spline_chart,
        'start_date': start_date,
        'end_date': end_date

    }
    return render(request, "user_own_report.html", context)