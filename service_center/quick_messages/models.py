from django.db import models
from django.core.urlresolvers import reverse
from phonenumber_field.modelfields import PhoneNumberField
# Create your models here.

class Templates(models.Model):

    template_name = models.CharField(max_length=120, unique=True)
    message = models.CharField(max_length=765, unique=True)

    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.template_name

    def get_absolute_url(self):
        return reverse("messages:quick_message_templates_list")

    class Meta:
        ordering = ["-timestamp"]


class Settings(models.Model):

    mob_no = PhoneNumberField()
    password = models.CharField(max_length=120)
    message = models.CharField(max_length=765)

    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.message

    def get_absolute_url(self):
        return reverse("messages:quick_message_settings")

