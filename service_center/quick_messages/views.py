from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core import serializers
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404, redirect

from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from customers.models import Customer
from .forms import message_form, TemplatesForm, SettingsForm
from .models import Templates, Settings

# Create your views here.

@login_required()
def quick_message(request):
    # print(request)
    if not request.user.is_superuser:
        raise Http404('User has no permission to do the following task !!!')

    form = message_form(request.POST or None)
    form.fields['phone_numbers'].choices =[(x.phone_number, x.full_name +'  (' +str(x.phone_number)+ ')') for x in Customer.objects.all()]

    templates = Templates.objects.all()

    context = {
        'form': form,
        'templates': templates

    }
    return render(request, "quick_message.html", context)

@login_required()
def quick_message_templates(request):
    # print(request)
    if not request.user.is_superuser:
        raise Http404('User has no permission to do the following task !!!')

    form = TemplatesForm(request.POST or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return HttpResponseRedirect(instance.get_absolute_url())

    context = {
        'form': form,
        'title': 'create message template'

    }
    return render(request, "message_template_form.html", context)

@login_required()
def quick_message_templates_list(request):
    if not request.user.is_superuser:
        raise Http404('User has no permission to do the following task !!!')

    queryset_list = Templates.objects.all()

    query = request.GET.get("query")
    if query:
        queryset_list = queryset_list.filter(
            Q(template_name__icontains=query) |
            Q(message__icontains=query) |
            Q(timestamp__icontains=query)
        ).distinct()
    paginator = Paginator(queryset_list, 10)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        query_set = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        query_set = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        query_set = paginator.page(paginator.num_pages)
    context = {
        "templates_list": query_set,
        "title": "template list",
        "templates_count": paginator.count
    }
    return render(request, "message_template_list.html", context)

@login_required()
def quick_message_templates_update(request, id=None):
    if not request.user.is_superuser:
        raise Http404('User has no permission to do the following task !!!')

    instance = get_object_or_404(Templates, id=id)
    form = TemplatesForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "template": instance,
        "title": "Edit template",
        "form": form,
    }
    return render(request, "message_template_form.html", context)

@login_required()
def quick_message_templates_delete(request, id=None):
    if not request.user.is_superuser:
        raise Http404('User has no permission to do the following task !!!')

    instance = get_object_or_404(Templates, id=id)
    instance.delete()

    return redirect("messages:quick_message_templates_list")

@login_required()
def quick_message_settings(request):
    if not request.user.is_superuser:
        raise Http404('User has no permission to do the following task !!!')

    try:
        credentials = Settings.objects.latest('updated')

        instance = get_object_or_404(Settings, id=credentials.id)
        form = SettingsForm(request.POST or None, instance=instance)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            return HttpResponseRedirect(instance.get_absolute_url())
    except:
        form = SettingsForm(request.POST or None)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            return HttpResponseRedirect(instance.get_absolute_url())

    context = {
        'form': form,
        'title': 'Default Message Settings'

    }
    return render(request, "message_settings_form.html", context)