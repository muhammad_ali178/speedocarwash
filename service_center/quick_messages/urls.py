from django.conf.urls import url

from .views import (
quick_message,
quick_message_templates,
quick_message_templates_list,
quick_message_templates_update,
quick_message_templates_delete,
quick_message_settings
)


urlpatterns = [
    url(r'^templates/create/$', quick_message_templates, name="quick_message_templates_create"),
    url(r'^settings/$', quick_message_settings, name="quick_message_settings"),
    url(r'^templates$', quick_message_templates_list, name="quick_message_templates_list"),
    url(r'^$', quick_message, name="quick_message"),
    url(r'^templates/(?P<id>\d+)/edit/$', quick_message_templates_update, name="quick_message_templates_update"),
    url(r'^templates/(?P<id>\d+)/delete/$', quick_message_templates_delete, name="quick_message_templates_delete"),
]