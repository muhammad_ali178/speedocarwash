from django.apps import AppConfig


class QuickMessagesConfig(AppConfig):
    name = 'quick_messages'
