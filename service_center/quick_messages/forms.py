from django import forms
from .models import Templates,Settings

class message_form(forms.Form):

    phone_numbers = forms.MultipleChoiceField()
    message = forms.CharField(
        required=True,
        widget=forms.Textarea
    )

class TemplatesForm(forms.ModelForm):
    message = forms.CharField(widget=forms.Textarea)
    class Meta:
        model = Templates

        fields = [
            "template_name",
            "message"
        ]


class SettingsForm(forms.ModelForm):
    message = forms.CharField(widget=forms.Textarea)

    class Meta:
        model = Settings

        fields = [
            "mob_no",
            "password",
            "message"
        ]
