from django import forms

from .models import Facilitie, Subfacilities


class FacilityForm(forms.ModelForm):
    class Meta:
        model = Facilitie

        fields = [
            "name",
            "description",
            "image",
            "activate"
        ]


class SubfacilitiesForm(forms.ModelForm):
    class Meta:
        model = Subfacilities

        fields = [
            "name",
            "price",
            "category_wise",
            'facility',
            'small_vehicle_price',
            'medium_vehicle_price',
            'large_vehicle_price',
            'bike_vehicle_price',
            "activate"
        ]

