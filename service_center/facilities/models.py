from django.db import models

from django.core.urlresolvers import reverse

# Create your models here.
class Facilitie(models.Model):

    name = models.CharField(max_length=120)
    description = models.TextField()
    image = models.ImageField(null=True, blank=True,
                              width_field="width_field",
                              height_field="height_field")
    activate = models.BooleanField(default=False)
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)

    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("facility:detail", kwargs={"id": self.id})

    class Meta:
        ordering = ["-timestamp"]


class Subfacilities(models.Model):
    name = models.CharField(max_length=120, unique=True)
    price = models.PositiveIntegerField(default=0, null=True, blank=True)
    small_vehicle_price = models.PositiveIntegerField(default=0, null=True, blank=True)
    medium_vehicle_price = models.PositiveIntegerField(default=0, null=True, blank=True)
    large_vehicle_price = models.PositiveIntegerField(default=0, null=True, blank=True)
    bike_vehicle_price = models.PositiveIntegerField(default=0, null=True, blank=True)
    category_wise = models.BooleanField(default=False)
    facility = models.ForeignKey(Facilitie, null=True, blank=True, on_delete=models.CASCADE)
    activate = models.BooleanField(default=False)

    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.name

    class Meta:
        ordering = ["-timestamp"]