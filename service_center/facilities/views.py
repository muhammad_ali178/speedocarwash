from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .models import Facilitie, Subfacilities

from django.contrib import messages

from .forms import FacilityForm, SubfacilitiesForm
from django.core import serializers
from django.http import JsonResponse
import json

@login_required()
def facility_create(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('facilities.add_facilitie'):
        raise Http404('User has no permission to do the following task !!!')
    form = FacilityForm(request.POST or None, request.FILES or None)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully created")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "form": form,
        "title": "Create Fcaility"

    }
    return render(request, "facility_form.html", context)


@login_required()
def facility_detail(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')

    instance = get_object_or_404(Facilitie, id=id)
    facility_id = instance.id

    #get subfacilities
    sub_facilities_list = Subfacilities.objects.filter(facility_id=facility_id)

    #subficility form
    sub_ficility_form = SubfacilitiesForm(request.POST or None)
    if request.method == "POST" and request.POST['action'] == 'subficility_form' and request.is_ajax():
        if sub_ficility_form.is_valid():
            obj = sub_ficility_form.save()
            return HttpResponse(serializers.serialize('json', [obj]), content_type="application/json")
        else:
            msg = "AJAX post invalid"
            return JsonResponse({'msg': msg})
    else:
        msg = "GET petitions are not allowed for this view."

    context = {
        "facility": instance,
        "title": "title list",
        "sub_ficility_form": sub_ficility_form,
        "facility_id": facility_id,
        "sub_facilities_list": sub_facilities_list
    }
    return render(request, "facility_detail.html", context)


@login_required()
def facility_list(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')

    # order form requests

    if request.is_ajax() and request.method == "GET" and request.GET['action'] == 'get_sub_facilities':
        result = Subfacilities.objects.filter(activate=1, facility_id=request.GET['selectedValues'])
        facility = Facilitie.objects.get(id=request.GET['selectedValues'])
        all_objects = list(result) + list([facility])
        return HttpResponse(serializers.serialize('json', result), content_type="application/json")
    else:
        msg = "GET petitions are not allowed for this view."

    if request.is_ajax() and request.method == "GET" and request.GET['action'] == 'getFacilities':

        already_selected_facility_list = request.GET['already_selected_facility_list']
        all_objects = Facilitie.objects.filter(activate=1).exclude(id__in=json.loads(already_selected_facility_list))
        print(all_objects)
        print("\n\n\n")
        return HttpResponse(serializers.serialize('json', all_objects), content_type="application/json")
    else:
        msg = "GET petitions are not allowed for this view."

    # edit form requests
    if request.is_ajax() and request.method == "GET" and request.GET['action'] == 'get_sub_facilitiesEdit':
        result = Subfacilities.objects.filter(facility_id=request.GET['selectedValues'])
        facility = Facilitie.objects.get(id=request.GET['selectedValues'])
        all_objects = list(result) + list([facility])
        return HttpResponse(serializers.serialize('json', result), content_type="application/json")
    else:
        msg = "GET petitions are not allowed for this view."

    if request.is_ajax() and request.method == "GET" and request.GET['action'] == 'getFacilitiesEdit':

        already_selected_facility_list = request.GET['already_selected_facility_list']
        all_objects = Facilitie.objects.all().exclude(id__in=json.loads(already_selected_facility_list))
        print(all_objects)
        print("\n\n\n")
        return HttpResponse(serializers.serialize('json', all_objects), content_type="application/json")
    else:
        msg = "GET petitions are not allowed for this view."
    # ---------------------------
    queryset_list = Facilitie.objects.all()

    query = request.GET.get("query")
    if query:
        queryset_list = queryset_list.filter(
            Q(name__icontains=query) |
            Q(description__icontains=query) |
            Q(timestamp__icontains=query)

        ).distinct()
    paginator = Paginator(queryset_list, 10)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        query_set = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        query_set = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        query_set = paginator.page(paginator.num_pages)
    context = {
        "facility_list": query_set,
        "title": "title list",
        "facility_count": paginator.count
    }
    return render(request, "facility_list.html", context)


@login_required()
def facility_update(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('facilities.change_facilitie'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(Facilitie, id=id)
    form = FacilityForm(request.POST or None, request.FILES or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "successfully updated")
        return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        "facility": instance,
        "title": "Edit Facility",
        "form": form,
    }
    return render(request, "facility_form.html", context)


@login_required()
def facility_delete(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('facilities.delete_facilitie'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(Facilitie, id=id)
    instance.delete()
    messages.success(request, "successfully deleted")
    return redirect("facility:list")

@login_required()
def delete_subFac(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('facilities.delete_subfacilities'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(Subfacilities, id=id)
    instance.delete()
    messages.success(request, "successfully deleted")
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

@login_required()
def subFac_update(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('facilities.change_subfacilities'):
        raise Http404('User has no permission to do the following task !!!')
    instance = get_object_or_404(Subfacilities, id=id)
    form = SubfacilitiesForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        super_facility_id = instance.facility.id
        super_facility = Facilitie.objects.get(id=super_facility_id)
        messages.success(request, "successfully updated")
        return HttpResponseRedirect(super_facility.get_absolute_url())
    context = {
        "facility": instance,
        "title": "Edit Sub-Facility",
        "form": form,
    }
    return render(request, "sub_facility_form.html", context)

