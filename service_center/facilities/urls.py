from django.conf.urls import url

from .views import (
    facility_create,
    facility_delete,
    facility_detail,
    facility_list,
    facility_update,

    delete_subFac,
    subFac_update
)


urlpatterns = [
    url(r'^create/$', facility_create, name="create"),
    url(r'^(?P<id>\d+)/$', facility_detail, name="detail"),
    url(r'^$', facility_list, name="list"),
    url(r'^(?P<id>\d+)/edit/$', facility_update, name="update"),
    url(r'^(?P<id>\d+)/delete/$', facility_delete, name="delete"),

    url(r'^sub/(?P<id>\d+)/delete/$', delete_subFac, name="delete_subFac"),
    url(r'^sub/(?P<id>\d+)/edit/$', subFac_update, name="subFac_update"),
]