from django.contrib import admin
from .models import Facilitie, Subfacilities

# Register your models here.


class FacilitiesModelAdmin(admin.ModelAdmin):
    list_display = ["name", "updated", "timestamp"]
    search_fields = ["name", "description"]

    class Meta:
        model = Facilitie

admin.site.register(Facilitie, FacilitiesModelAdmin)


class SubfacilitiesModelAdmin(admin.ModelAdmin):
    list_display = ["name", "updated", "timestamp"]
    search_fields = ["name"]

    class Meta:
        model = Subfacilities

admin.site.register(Subfacilities, SubfacilitiesModelAdmin)
