from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.http import JsonResponse
from django.db.models import Q
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import F
from .models import Order
from cars.models import Car
from quick_messages.models import Settings
from facilities.models import Facilitie, Subfacilities
from discounts.models import Discount

from django.contrib import messages

from .forms import OrderForm
from customers.forms import CustomerForm
from cars.forms import CarForm
import json

from django.core import serializers

import datetime
import itertools
from collections import defaultdict
from django.views.decorators.csrf import csrf_exempt
from collections import Counter
from django.db.models.functions import TruncMonth
from django.db.models import Count
# Create your views here.


@login_required()
def order_create(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('order.add_order'):
        raise Http404('User has no permission to do the following task !!!')
    form = OrderForm(request.POST or None)

    if form.is_valid():
        # ----------------- token number settings -------------------
        try:
            latest_order = Order.objects.values('timestamp', 'token_number').latest('timestamp')
            latest_previous_order_date = latest_order['timestamp']
            previous_token_number = latest_order['token_number']
            latest_previous_date = latest_previous_order_date.date()
        except Order.DoesNotExist:
            previous_token_number = 0
            latest_previous_date = datetime.date.today()
        today_date = datetime.date.today()

        # -----------------------------------------------------------
        instance = form.save()
        instance.created_user = request.user

        facilities_and_subfacilities_id = json.loads(instance.facility_and_prices)

        list_of_facilities = list(itertools.chain.from_iterable(facilities_and_subfacilities_id['facilities']))
        facilites_list = Facilitie.objects.filter(id__in=list_of_facilities)
        instance.facility = facilites_list
        # print(facilites_list)

        list_of_subfacilities = list(itertools.chain.from_iterable(facilities_and_subfacilities_id['subFacilities']))
        choosen_facility_category_of_vehicle = instance.vehicle.category

        subfacilities_list = Subfacilities.objects.filter(id__in=list_of_subfacilities)
        # print(subfacilities_list)

        # -----------------------------------------------------------
        try:
            matching_discount = Discount.objects.get(activate=1, visit_count=instance.vehicle.visit_count + 1)
            matching_discount_existance = True
            # declare_discount_string = "(After " + str(matching_discount.discount_percentage) + " % Discount)"
            instance.declare_discount_string = matching_discount.discount_percentage
        except Discount.DoesNotExist:
            matching_discount_existance = False
            # declare_discount_string = ""
            # instance.declare_discount_string = declare_discount_string
        # -----------------------------------------------------------

        total = 0
        data_dict = {}
        data_dict_array = {}
        for sub_facility in subfacilities_list:
            if sub_facility.category_wise == 1:
                if choosen_facility_category_of_vehicle == 'S':
                    if matching_discount_existance == True:
                        calculate_the_savings = (matching_discount.discount_percentage / 100) * sub_facility.small_vehicle_price
                        subtract_saving_from_orignal = sub_facility.small_vehicle_price - calculate_the_savings

                        total += round(subtract_saving_from_orignal)
                        data_dict[sub_facility.name] = round(subtract_saving_from_orignal)
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id': sub_facility.id, 'sub_facility_name': sub_facility.name,'price': round(subtract_saving_from_orignal),'discounted':'true'});
                        # ---------------------------------------------------
                    else:
                        total += sub_facility.small_vehicle_price
                        data_dict[sub_facility.name] = sub_facility.small_vehicle_price
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name, 'id': sub_facility.facility_id , 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id':sub_facility.id, 'sub_facility_name':sub_facility.name, 'price': sub_facility.small_vehicle_price,'discounted':'false'});
                        # ---------------------------------------------------

                elif choosen_facility_category_of_vehicle == 'M':
                    if matching_discount_existance == True:
                        calculate_the_savings = (matching_discount.discount_percentage / 100) * sub_facility.medium_vehicle_price
                        subtract_saving_from_orignal = sub_facility.medium_vehicle_price - calculate_the_savings

                        total += round(subtract_saving_from_orignal)
                        data_dict[sub_facility.name] = round(subtract_saving_from_orignal)
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id': sub_facility.id, 'sub_facility_name': sub_facility.name,'price': round(subtract_saving_from_orignal),'discounted':'true'});
                        # ---------------------------------------------------

                    else:
                        total += sub_facility.medium_vehicle_price
                        data_dict[sub_facility.name] = sub_facility.medium_vehicle_price
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id':sub_facility.id,'sub_facility_name': sub_facility.name, 'price': sub_facility.medium_vehicle_price,'discounted':'false'});
                        # ---------------------------------------------------

                elif choosen_facility_category_of_vehicle == 'B':
                    if matching_discount_existance == True:
                        calculate_the_savings = (matching_discount.discount_percentage / 100) * sub_facility.bike_vehicle_price
                        subtract_saving_from_orignal = sub_facility.bike_vehicle_price - calculate_the_savings

                        total += round(subtract_saving_from_orignal)
                        data_dict[sub_facility.name] = round(subtract_saving_from_orignal)
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id': sub_facility.id, 'sub_facility_name': sub_facility.name,'price': round(subtract_saving_from_orignal),'discounted':'true'});
                        # ---------------------------------------------------

                    else:
                        total += sub_facility.bike_vehicle_price
                        data_dict[sub_facility.name] = sub_facility.bike_vehicle_price
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id':sub_facility.id,'sub_facility_name': sub_facility.name, 'price': sub_facility.bike_vehicle_price,'discounted':'false'});
                        # ---------------------------------------------------
                else:
                    if matching_discount_existance == True:
                        calculate_the_savings = (matching_discount.discount_percentage / 100) * sub_facility.large_vehicle_price
                        subtract_saving_from_orignal = sub_facility.large_vehicle_price - calculate_the_savings

                        total += round(subtract_saving_from_orignal)
                        data_dict[sub_facility.name] = round(subtract_saving_from_orignal)
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id': sub_facility.id, 'sub_facility_name': sub_facility.name,'price': round(subtract_saving_from_orignal),'discounted':'true'});
                        # ---------------------------------------------------

                    else:
                        total += sub_facility.large_vehicle_price
                        data_dict[sub_facility.name] = sub_facility.large_vehicle_price
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id':sub_facility.id,'sub_facility_name': sub_facility.name, 'price': sub_facility.large_vehicle_price,'discounted':'false'});
                        # ---------------------------------------------------
            else:
                if matching_discount_existance == True:
                    calculate_the_savings = (matching_discount.discount_percentage / 100) * sub_facility.price
                    subtract_saving_from_orignal = sub_facility.price - calculate_the_savings

                    total += subtract_saving_from_orignal
                    data_dict[sub_facility.name] = subtract_saving_from_orignal
                    # ---------------------------------------------------
                    if not sub_facility.facility_id in data_dict_array:
                        data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                    data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id': sub_facility.id, 'sub_facility_name': sub_facility.name,'price': subtract_saving_from_orignal,'discounted':'true'});
                    # ---------------------------------------------------
                else:
                    total += sub_facility.price
                    data_dict[sub_facility.name] = sub_facility.price
                    # ---------------------------------------------------
                    if not sub_facility.facility_id in data_dict_array:
                        data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                    data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id':sub_facility.id,'sub_facility_name': sub_facility.name, 'price': sub_facility.price,'discounted':'false'});
                    # ---------------------------------------------------

        instance.total_amount = total
        print(data_dict_array)
        # print(data_dict)
        print("\n\n\n")

        # ------------------ save token number setting -----------------
        if today_date == latest_previous_date:
            print('true')
            instance.token_number = previous_token_number + 1
        else:
            print('false')
            instance.token_number = 1
        # --------------------------------------------------------------
        instance.visit_car_count = instance.vehicle.visit_count + 1
        instance.save()
        # Order.objects.filter(id=instance.id).update(token_number=instance.id)
        Car.objects.filter(id=instance.vehicle.id).update(visit_count=F('visit_count') + 1)
        Order.objects.filter(id=instance.id).update(facility_and_prices=data_dict_array)

        messages.success(request, "successfully created")
        return HttpResponseRedirect(instance.get_absolute_url())


    form_customer = CustomerForm(request.POST or None)
    if request.is_ajax() and request.POST['action'] == 'customer_form' and request.method == "POST":
        if form_customer.is_valid():
            obj = form_customer.save()
            return HttpResponse(serializers.serialize('json', [obj]), content_type="application/json")
        else:
            return JsonResponse({'errors': form_customer.errors})
    else:
        msg = "GET petitions are not allowed for this view."
        print(msg)

    form_car = CarForm(request.POST or None)
    if request.is_ajax() and request.POST['action'] == 'car_form' and request.method == "POST":
        if form_car.is_valid():
            obj_car = form_car.save()
            return HttpResponse(serializers.serialize('json', [obj_car]), content_type="application/json")
        else:
            msg = "AJAX post invalid"
            return JsonResponse({'errors': form_car.errors})
    else:
        msg = "GET petitions are not allowed for this view."

    context = {
        "form": form,
        "form_customer": form_customer,
        "form_car": form_car
    }
    return render(request, "order_form.html", context)


@login_required()
def order_detail(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    instance = get_object_or_404(Order, id=id)
    data_string = instance.facility_and_prices
    str1 = str(data_string)
    data_dict = eval(str1)

    # message_credentials

    message_credentials = Settings.objects.latest('updated')

    context = {
        "order": instance,
        "title": "title list",
        "facility_dict": data_dict,
        "message_credentials": message_credentials
    }
    return render(request, "order_detail.html", context)


@csrf_exempt
@login_required()
def order_list(request):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')

    # message_credentials

    message_credentials = Settings.objects.latest('updated')

    if request.is_ajax() and request.method == "POST":
        if request.POST['action'] == 'order_approve':
            complete = True
        if request.POST['action'] == 'order_not_approve':
            complete = False

        order_id = request.POST['order_id']
        Order.objects.filter(id=order_id).update(approved=complete)
        return HttpResponse("Success")

    queryset_list = Order.objects.all()

    query = request.GET.get("query")
    if query:
        queryset_list = queryset_list.filter(
            Q(customer__full_name__contains=query) |
            Q(customer__phone_number__contains=query) |
            Q(facility__name__contains=query) |
            Q(vehicle__model_code__contains=query) |
            Q(timestamp__icontains=query)
        ).distinct()
    paginator = Paginator(queryset_list, 15)  # Show 25 contacts per page

    page = request.GET.get('page')
    try:
        query_set = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        query_set = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        query_set = paginator.page(paginator.num_pages)
    context = {
        "order_list": query_set,
        "title": "title list",
        "message_credentials": message_credentials,
        "order_count": paginator.count
    }
    return render(request, "order_list.html", context)


@login_required()
def order_update(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('order.change_order'):
        raise Http404('User has no permission to do the following task !!!')

    instance = get_object_or_404(Order, id=id)

    fac_and_sub_list = instance.facility_and_prices
    fac_and_sub_dict = eval(fac_and_sub_list)

    form = OrderForm(request.POST or None, instance=instance)
    if form.is_valid():
        instance = form.save()

        facilities_and_subfacilities_id = json.loads(instance.facility_and_prices)

        list_of_facilities = list(itertools.chain.from_iterable(facilities_and_subfacilities_id['facilities']))
        facilites_list = Facilitie.objects.filter(id__in=list_of_facilities)
        instance.facility = facilites_list

        list_of_subfacilities = list(itertools.chain.from_iterable(facilities_and_subfacilities_id['subFacilities']))
        choosen_facility_category_of_vehicle = instance.vehicle.category

        subfacilities_list = Subfacilities.objects.filter(id__in=list_of_subfacilities)

        if instance.declare_discount_string != None:
            matching_discount_existance = True
        else:
            matching_discount_existance = False

        total = 0
        data_dict = {}
        data_dict_array = {}
        for sub_facility in subfacilities_list:
            if sub_facility.category_wise == 1:
                if choosen_facility_category_of_vehicle == 'S':

                    if matching_discount_existance == True:
                        calculate_the_savings = (instance.declare_discount_string / 100) * sub_facility.small_vehicle_price
                        subtract_saving_from_orignal = sub_facility.small_vehicle_price - calculate_the_savings

                        total += round(subtract_saving_from_orignal)
                        data_dict[sub_facility.name] = round(subtract_saving_from_orignal)
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id': sub_facility.id, 'sub_facility_name': sub_facility.name,'price': round(subtract_saving_from_orignal)});
                        # ---------------------------------------------------

                    else:
                        total += sub_facility.small_vehicle_price
                        data_dict[sub_facility.name] = sub_facility.small_vehicle_price
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,
                                                                         'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append(
                            {'sub_facility_id':sub_facility.id,'sub_facility_name': sub_facility.name, 'price': sub_facility.small_vehicle_price});
                        # ---------------------------------------------------

                elif choosen_facility_category_of_vehicle == 'M':

                    if matching_discount_existance == True:
                        calculate_the_savings = (instance.declare_discount_string / 100) * sub_facility.medium_vehicle_price
                        subtract_saving_from_orignal = sub_facility.medium_vehicle_price - calculate_the_savings

                        total += round(subtract_saving_from_orignal)
                        data_dict[sub_facility.name] = round(subtract_saving_from_orignal)
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id': sub_facility.id, 'sub_facility_name': sub_facility.name,'price': round(subtract_saving_from_orignal)});
                        # ---------------------------------------------------

                    else:
                        total += sub_facility.medium_vehicle_price
                        data_dict[sub_facility.name] = sub_facility.medium_vehicle_price
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,
                                                                         'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append(
                            {'sub_facility_id':sub_facility.id,'sub_facility_name': sub_facility.name, 'price': sub_facility.medium_vehicle_price});
                        # ---------------------------------------------------

                elif choosen_facility_category_of_vehicle == 'B':
                    if matching_discount_existance == True:
                        calculate_the_savings = (instance.declare_discount_string / 100) * sub_facility.bike_vehicle_price
                        subtract_saving_from_orignal = sub_facility.bike_vehicle_price - calculate_the_savings

                        total += round(subtract_saving_from_orignal)
                        data_dict[sub_facility.name] = round(subtract_saving_from_orignal)
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id': sub_facility.id, 'sub_facility_name': sub_facility.name,'price': round(subtract_saving_from_orignal)});
                        # ---------------------------------------------------
                    else:
                        total += sub_facility.bike_vehicle_price
                        data_dict[sub_facility.name] = sub_facility.bike_vehicle_price
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,
                                                                         'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append(
                            {'sub_facility_id':sub_facility.id,'sub_facility_name': sub_facility.name, 'price': sub_facility.bike_vehicle_price});
                        # ---------------------------------------------------
                else:
                    if matching_discount_existance == True:
                        calculate_the_savings = (instance.declare_discount_string / 100) * sub_facility.large_vehicle_price
                        subtract_saving_from_orignal = sub_facility.large_vehicle_price - calculate_the_savings

                        total += round(subtract_saving_from_orignal)
                        data_dict[sub_facility.name] = round(subtract_saving_from_orignal)
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id': sub_facility.id, 'sub_facility_name': sub_facility.name,'price': round(subtract_saving_from_orignal)});
                        # ---------------------------------------------------
                    else:
                        total += sub_facility.large_vehicle_price
                        data_dict[sub_facility.name] = sub_facility.large_vehicle_price
                        # ---------------------------------------------------
                        if not sub_facility.facility_id in data_dict_array:
                            data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,
                                                                         'id': sub_facility.facility_id, 'sub': []};
                        data_dict_array[sub_facility.facility_id]['sub'].append(
                            {'sub_facility_id':sub_facility.id,'sub_facility_name': sub_facility.name, 'price': sub_facility.large_vehicle_price});
                        # ---------------------------------------------------
            else:
                if matching_discount_existance == True:
                    calculate_the_savings = (instance.declare_discount_string / 100) * sub_facility.price
                    subtract_saving_from_orignal = sub_facility.price - calculate_the_savings

                    total += round(subtract_saving_from_orignal)
                    data_dict[sub_facility.name] = round(subtract_saving_from_orignal)
                    # ---------------------------------------------------
                    if not sub_facility.facility_id in data_dict_array:
                        data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,'id': sub_facility.facility_id, 'sub': []};
                    data_dict_array[sub_facility.facility_id]['sub'].append({'sub_facility_id': sub_facility.id, 'sub_facility_name': sub_facility.name,'price': round(subtract_saving_from_orignal)});
                    # ---------------------------------------------------
                else:
                    total += sub_facility.price
                    data_dict[sub_facility.name] = sub_facility.price
                    # ---------------------------------------------------
                    if not sub_facility.facility_id in data_dict_array:
                        data_dict_array[sub_facility.facility_id] = {'name': sub_facility.facility.name,
                                                                     'id': sub_facility.facility_id, 'sub': []};
                    data_dict_array[sub_facility.facility_id]['sub'].append(
                        {'sub_facility_id':sub_facility.id,'sub_facility_name': sub_facility.name, 'price': sub_facility.price});
                    # ---------------------------------------------------

        instance.total_amount = total

        instance.save()

        Order.objects.filter(id=instance.id).update(facility_and_prices=data_dict_array)

        messages.success(request, "successfully updated")
        return HttpResponseRedirect(instance.get_absolute_url())

    form_customer = CustomerForm(request.POST or None)
    if request.method == "POST" and request.POST['action'] == 'customer_form' and request.is_ajax():
        if form_customer.is_valid():
            obj = form_customer.save()
            return HttpResponse(serializers.serialize('json', [obj]), content_type="application/json")
        else:
            return JsonResponse({'errors': form_customer.errors})
    else:
        msg = "GET petitions are not allowed for this view."

    form_car = CarForm(request.POST or None)
    if request.method == "POST" and request.POST['action'] == 'car_form' and request.is_ajax():
        if form_car.is_valid():
            obj_car = form_car.save()
            return HttpResponse(serializers.serialize('json', [obj_car]), content_type="application/json")
        else:
            return JsonResponse({'errors': form_car.errors})
    else:
        msg = "GET petitions are not allowed for this view."

    context = {
        "order": instance,
        "title": "title list",
        "form": form,
        "form_customer": form_customer,
        "form_car": form_car,
        "fac_and_sub_dict": fac_and_sub_dict
    }
    return render(request, "order_edit_form.html", context)


@login_required()
def order_delete(request, id=None):
    if not request.user.is_staff:
        raise Http404('User is not authorized by admin !!!')
    if not request.user.has_perm('order.delete_order'):
        raise Http404('User has no permission to do the following task !!!')
    if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    instance = get_object_or_404(Order, id=id)
    instance.delete()
    messages.success(request, "successfully deleted")
    return redirect("order:list")

