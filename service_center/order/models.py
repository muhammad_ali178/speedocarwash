from django.db import models
from customers.models import Customer
from facilities.models import Facilitie
from cars.models import Car

from django.conf import settings

from django.core.urlresolvers import reverse

# Create your models here.


class Order(models.Model):
    created_user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1)
    token_number = models.IntegerField(default=0)
    visit_car_count = models.IntegerField(default=0)
    customer = models.ForeignKey(Customer)
    facility = models.ManyToManyField(Facilitie, related_name='facility')
    vehicle = models.ForeignKey(Car)
    owner_reference = models.CharField(max_length=50, null=True, blank=True)
    total_amount = models.IntegerField(default=0)
    approved = models.BooleanField(default=False)
    facility_and_prices = models.TextField()
    declare_discount_string = models.IntegerField(null=True, blank=True)

    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __str__(self):
        return self.customer.full_name

    def get_absolute_url(self):
        return reverse("order:detail", kwargs={"id": self.id})

    class Meta:
        ordering = ["-timestamp"]

