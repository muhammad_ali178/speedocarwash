from django.contrib import admin
from .models import Order
# Register your models here.


class OrderModelAdmin(admin.ModelAdmin):
    list_display = ["id", "customer", "vehicle", "owner_reference", "updated", "timestamp"]
    search_fields = ["customer__full_name", "vehicle__model_code"]

    class Meta:
        model = Order

admin.site.register(Order, OrderModelAdmin)