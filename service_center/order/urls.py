from django.conf.urls import url

from .views import (
    order_create,
    order_delete,
    order_detail,
    order_list,
    order_update,
)


urlpatterns = [
    url(r'^create/$', order_create, name="create"),
    url(r'^(?P<id>\d+)/$', order_detail, name="detail"),
    url(r'^$', order_list, name="list"),
    url(r'^(?P<id>\d+)/edit/$', order_update, name="update"),
    url(r'^(?P<id>\d+)/delete/$', order_delete, name="delete"),
]