from django import forms

from .models import Order

class OrderForm(forms.ModelForm):
    class Meta:
        model = Order

        fields = [
            "customer",
            "vehicle",
            "owner_reference",
            # "facility",
            "facility_and_prices"
        ]
