# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-11-21 13:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0003_auto_20161005_1811'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='declare_discount_string',
            field=models.CharField(blank=True, max_length=200, null=True),
        ),
    ]
