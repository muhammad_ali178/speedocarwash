from django.db import models

from django.core.urlresolvers import reverse

# Create your models here.

class Discount(models.Model):

    visit_count = models.PositiveIntegerField(unique=True)
    discount_percentage = models.PositiveIntegerField()
    activate = models.BooleanField(default=False)

    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)

    def __int__(self):
        return self.visit_count

    def get_absolute_url(self):
        return reverse("discounts:discounts_list")

    class Meta:
        ordering = ["-timestamp"]