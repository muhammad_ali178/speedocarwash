from django import forms

from .models import Discount


class DiscountForm(forms.ModelForm):
    class Meta:
        model = Discount

        fields = [
            "visit_count",
            "discount_percentage",
            "activate"
        ]