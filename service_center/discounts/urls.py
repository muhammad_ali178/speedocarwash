from django.conf.urls import url

from .views import (
   discount_create,
    discounts_list,
    discount_update,
    discounts_delete
)


urlpatterns = [
    url(r'^create/$', discount_create, name="discounts_create"),
    url(r'^$', discounts_list, name="discounts_list"),
    url(r'^(?P<id>\d+)/edit/$', discount_update, name="discounts_update"),
    url(r'^(?P<id>\d+)/delete/$', discounts_delete, name="discounts_delete")
]